package com.example.commande;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.ClientService;
import com.example.commande.service.CommandService;
import com.example.commande.tableClass.Client;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.Table;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_ISCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_configure_template.EXTRA_1;
import static com.example.commande.activity_configure_template.EXTRA_2;
import static com.example.commande.activity_configure_template.EXTRA_3;
import static com.example.commande.activity_configure_template.EXTRA_NAME;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

public class activity_orders_list extends AppCompatActivity {
    public static String EXTRA_ID_COMMAND = "com.example.commande.EXTRA_ID_COMMAND";
    private CommandService commandService;
    private ClientService clientService;
    private int idCommand;
    private ListView list;
    private TextView Text2;
    private TextView Text3;
    private TextView Text4;
    private int indexClientId;
    private String index1;
    private String index2;
    List<Table> clients = new ArrayList<>();
    List<Command> commands = new ArrayList<>();
    boolean get;
    private String url;
    int indexCommercant;
    String name;
    String msg1;
    String msg2;
    String msg3;
    int idTemplate;
    private ImageView imageView;
    private String notificationNameCommand = "New Command !";
    private String notificationDescriptionCommand = "There is a new command for your shop !";

    private void createNotificationChannel2() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_orders_list", notificationNameCommand, importance);
            channel.setDescription(notificationDescriptionCommand);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void AlertCommand(){
        commandService.getCommand(-1, callBackCommandNotif);
    }

    class OrderList  extends BaseAdapter {
        List<Command> dataSource;

        public OrderList(List<Command> data) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Command> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateCityList(List<Command> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Command c = dataSource.get(position);
            indexClientId = c.getIdClient();

            convertView = LayoutInflater.from(activity_orders_list.this).inflate(R.layout.fragment_commandlist_item,null);

            TextView Text1 = (TextView) convertView.findViewById(R.id.textView5);
            Text2 = (TextView) convertView.findViewById(R.id.textView7);
            Text3 = (TextView) convertView.findViewById(R.id.textView8);
            Text4 = (TextView) convertView.findViewById(R.id.textView9);



            if (c.getStatus().equals("send")) {
                if (c.getLivraison() == 1)
                    Text1.setText("Command id : " + commands.get(position).getId() + " Price : " + c.getPrice() + " SEND by poste");
                if (c.getLivraison() == 2)
                    Text1.setText("Command id : " + commands.get(position).getId() + " Price : " + c.getPrice() + " SEND by delivery guy");
                if (c.getLivraison() == 3)
                    Text1.setText("Command id : " + commands.get(position).getId() + " Price : " + c.getPrice() + " SEND by relay point");
            }else{
                if (c.getLivraison() == 1)
                    Text1.setText("Command id : " + commands.get(position).getId() + " Price : " + c.getPrice() + " by poste");
                if (c.getLivraison() == 2)
                    Text1.setText("Command id : " + commands.get(position).getId() + " Price : " + c.getPrice() + " by delivery guy");
                if (c.getLivraison() == 3)
                    Text1.setText("Command id : " + commands.get(position).getId() + " Price : " + c.getPrice() + " by relay point");
            }

            get = false;
            for (Table t:clients
            ) {
                Client ce = (Client) t;
                if(ce.getId() == c.getIdClient()){
                    Text2.setText("First Name : " + ce.getName());
                    Text3.setText("Last Name : " + ce.getSurname());
                    Text4.setText("Addresse : " + ce.getAddresse());
                    get = true;
                    break;
                }
            }

            Button button = convertView.findViewById(R.id.button66);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (c.getStatus().equals("en cours de livraison"))
                        commandService.putCommand(new Command(c.getId(),null, 0, "send", 0,0, 0, 0 ),callBackCommandSend );
                }
            });

            return convertView;
        }
    }
    OrderList OL=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);
        imageView = findViewById(R.id.imageView77);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
        index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
        name = bundle.get(EXTRA_NAME).toString();
        msg1 = bundle.get(EXTRA_1).toString();
        msg2 = bundle.get(EXTRA_2).toString();
        msg3 = bundle.get(EXTRA_3).toString();
        idTemplate = (int) bundle.get(EXTRA_ID_TEMPLATE);
        url = bundle.get(EXTRA_URL).toString();
        if(index1 != null){
            indexCommercant = Integer.valueOf(index1);
        }else{
            indexCommercant = Integer.valueOf(index2);
        }

        commandService = new CommandService(this, url);
        clientService = new ClientService(this, url);
        list = (ListView) findViewById(R.id.listview);

        clientService.getClient(-1, callBackClient);
        AlertCommand();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_company_page.class);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_NAME, name);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                intent.putExtra(EXTRA_ISCOMMERCANT, 1);
                startActivityForResult(intent,1);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {

                final Intent intent = new Intent(getApplicationContext(), activity_command.class);
                final com.example.commande.tableClass.Command item = (com.example.commande.tableClass.Command) parent.getItemAtPosition(position);
                intent.putExtra(com.example.commande.tableClass.Command.TAG, item );
                intent.putExtra(EXTRA_ID_COMMAND, commands.get(position).getId());
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_NAME, name);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                startActivityForResult(intent,1);
            }
        });

    }

    private final CallBack callBackCommandSend = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Toast.makeText(activity_orders_list.this, "Command Send", Toast.LENGTH_SHORT).show();
            clientService.getClient(-1, callBackClient);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("Client ERROR", exception.toString());
        }
    };

    private final CallBack callBackClient = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            clients = table;
            commandService.getCommand(-1, callBackCommand);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("Client ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommand = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            ArrayList<Command> ArrOff=new ArrayList<>();
            for (Table t: table
            ) {
                Command d = (Command) t;
                if(d.getIdShop() == indexCommercant && !d.getStatus().equals("disponible")) {
                    ArrOff.add(d);
                    commands.add(d);
                }
            }

            OL=new OrderList(ArrOff);
            list.setAdapter(OL);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("Command ERROR", exception.toString());
        }
    };

    ////////////////////////////////Notification Command///////////////////////////////////////

    private final CallBack callBackCommandNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Command c = (Command) t;
                if(c.getNotif() == 1){
                    if (c.getIdShop() == Integer.valueOf(index1)){
                        createNotificationChannel2();//create notification offer for client
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_orders_list" )
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle(notificationNameCommand)
                                .setContentText(notificationDescriptionCommand)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                        // notificationId is a unique int for each notification that you must define
                        notificationManager.notify(14, builder.build());
                        commandService.putCommand(new Command(c.getId(), null, -1, null, -1, -1, 0, 0),callBackCommandNotifPut );
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandNotifPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}