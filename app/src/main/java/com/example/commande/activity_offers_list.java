package com.example.commande;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.Table;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_ISCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_configure_template.EXTRA_1;
import static com.example.commande.activity_configure_template.EXTRA_2;
import static com.example.commande.activity_configure_template.EXTRA_3;
import static com.example.commande.activity_configure_template.EXTRA_NAME;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

public class activity_offers_list extends AppCompatActivity {
    public static String EXTRA_POSITION = "com.example.commande.EXTRA_POSITION";
    private OfferService  offerService;
    private ProductService productService;
    private ListView list;
    private String index1;
    private String index2;
    private ImageView imageView;
    private String url;
    String name;
    String msg1;
    String msg2;
    String msg3;
    int idTemplate;
    private ArrayList<Offer> ArrOff;
    int indexCommercant;
    private List<Product> products;
    private CommandService commandService;
    private String notificationNameCommand = "New Command !";
    private String notificationDescriptionCommand = "There is a new command for your shop !";

    private void createNotificationChannel2() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_offers_list", notificationNameCommand, importance);
            channel.setDescription(notificationDescriptionCommand);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void AlertCommand(){
        commandService.getCommand(-1, callBackCommandNotif);
    }

    class OfferList  extends BaseAdapter {
        List<Offer> dataSource;

        public OfferList(List<Offer> data) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Offer> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateCityList(List<Offer> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Offer c = dataSource.get(position);
            Bundle bundle = new Bundle();


            convertView = LayoutInflater.from(activity_offers_list.this).inflate(R.layout.fragment_offerlist_item,null);
            ImageView delete = convertView.findViewById(R.id.imageView12);
            ImageView modife = convertView.findViewById(R.id.imageView9);


            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    offerService.putOffer(new Offer(c.getId(), "rien", 0, 0, 0, indexCommercant, 0 ),callBackOffer2 );
                }
            });

            modife.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent intent = new Intent(getApplicationContext(), activity_adding_stuff.class);
                    intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                    intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                    intent.putExtra(EXTRA_URL, url);
                    intent.putExtra(EXTRA_NAME, name);
                    intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                    intent.putExtra(EXTRA_1, msg1);
                    intent.putExtra(EXTRA_2, msg2);
                    intent.putExtra(EXTRA_3, msg3);
                    intent.putExtra(EXTRA_POSITION, c.getId());
                    startActivityForResult(intent,1);
                }
            });

            TextView Text = (TextView) convertView.findViewById(R.id.text2);
            TextView offer = convertView.findViewById(R.id.text1);

            String Name="";
            String offertext ="Offer on ";


            if(c.getOfferType()==2)
            {
                for (Product p: products
                     ) {
                    if (c.getId() == p.getOfferId()){
                        offertext+=+c.getValue1()+"% on ";
                        Name+=p.getName();
                        Text.setText(Name);
                        offer.setText(offertext);
                    }
                }

            }
            else if (c.getOfferType() == 1 )
            {
                for (Product p: products
                ) {
                    if (c.getId() == p.getOfferId()) {
                        Name += "Get " + c.getValue1() + " Bying ";
                        Name += c.getValue2();
                        offertext +=p.getName();
                        Text.setText(Name);
                        offer.setText(offertext);
                    }
                }
            }

            return convertView;
        }
    }
    OfferList OL=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers_list);

        imageView = findViewById(R.id.imageView78);
        list = (ListView) findViewById(R.id.listview);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
        index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
        name = bundle.get(EXTRA_NAME).toString();
        msg1 = bundle.get(EXTRA_1).toString();
        msg2 = bundle.get(EXTRA_2).toString();
        msg3 = bundle.get(EXTRA_3).toString();
        idTemplate = (int) bundle.get(EXTRA_ID_TEMPLATE);

        url = bundle.get(EXTRA_URL).toString();
        offerService = new OfferService(this, url);
        productService = new ProductService(this, url);
        commandService = new CommandService(this, url);
        AlertCommand();

        if(index1 != null){
            indexCommercant = Integer.valueOf(index1);
        }else{
            indexCommercant = Integer.valueOf(index2);
        }
        offerService.getOffer(-1,callBackOffer);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_company_page.class);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_NAME, name);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                intent.putExtra(EXTRA_ISCOMMERCANT, 1);
                startActivityForResult(intent,1);
            }
        });
    }

    private final CallBack callBackOffer = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            ArrOff=new ArrayList<>();
            products = new ArrayList<>();
            for (Table t: table
                 ) {
                Offer o = (Offer) t;
                if(o.getShopId() == indexCommercant && o.getOfferType() != 0)
                    ArrOff.add(o);
            }
            productService.getProduct(-1, callBackProduct);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Product o = (Product) t;
                products.add(o);
            }
            OL=new OfferList(ArrOff);
            list.setAdapter(OL);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOffer2 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Toast.makeText(activity_offers_list.this, "Offer deleted", Toast.LENGTH_SHORT).show();
            final Intent intent = new Intent(getApplicationContext(), activity_offers_list.class);
            intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
            intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
            intent.putExtra(EXTRA_URL, url);
            intent.putExtra(EXTRA_NAME, name);
            intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
            intent.putExtra(EXTRA_1, msg1);
            intent.putExtra(EXTRA_2, msg2);
            intent.putExtra(EXTRA_3, msg3);
            startActivityForResult(intent,1);

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER2 ERROR", exception.toString());
        }
    };

    ////////////////////////////////Notification Command///////////////////////////////////////

    private final CallBack callBackCommandNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Command c = (Command) t;
                if(c.getNotif() == 1){
                    if (c.getIdShop() == Integer.valueOf(index1)){
                        createNotificationChannel2();//create notification offer for client
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_offers_list" )
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle(notificationNameCommand)
                                .setContentText(notificationDescriptionCommand)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                        // notificationId is a unique int for each notification that you must define
                        notificationManager.notify(13, builder.build());
                        commandService.putCommand(new Command(c.getId(), null, -1, null, -1, -1, 0, 0),callBackCommandNotifPut );
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandNotifPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}
