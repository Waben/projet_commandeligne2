package com.example.commande;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandListService;
import com.example.commande.service.CommandService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductPictureService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.CommandList;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.ProductPicture;
import com.example.commande.tableClass.Table;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_ISCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_client_search.EXTRA_ID_PRODUCT2;
import static com.example.commande.activity_configure_template.EXTRA_1;
import static com.example.commande.activity_configure_template.EXTRA_2;
import static com.example.commande.activity_configure_template.EXTRA_3;
import static com.example.commande.activity_configure_template.EXTRA_NAME;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

public class activity_search_product_commercant extends AppCompatActivity {
    private TextView ArticleName;
    private TextView ArticleStatus;
    private TextView ArticlePrice;
    private ImageView ImageProduct;
    private Button Available;
    private Button UNAVAILABLE;
    private Button delete;
    private String url;
    private int idProduct;
    private ProductService productService;
    private Product currentProduct;
    private EditText editText;
    private int available;
    private ImageView imageView;
    private String index1;
    private String index2;
    private String name;
    private String msg1;
    private String msg2;
    private String msg3;
    private int idTemplate;
    private EditText newprice;
    private Button buttonPrice;
    private ProductPictureService productPictureService;
    private ImageView picture;
    private CommandService commandService;
    private String notificationNameCommand = "New Command !";
    private String notificationDescriptionCommand = "There is a new command for your shop !";

    private void createNotificationChannel2() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_search_product_commercant", notificationNameCommand, importance);
            channel.setDescription(notificationDescriptionCommand);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void AlertCommand(){
        commandService.getCommand(-1, callBackCommandNotif);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product_commercant);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();


        idProduct = (int) bundle.get(EXTRA_ID_PRODUCT2);
        url = bundle.get(EXTRA_URL).toString();
        index1 = bundle.get(EXTRA_INDEX_COMMERCANT).toString();
        index2 = bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
        name = bundle.get(EXTRA_NAME).toString();
        idTemplate = (int)bundle.get(EXTRA_ID_TEMPLATE);
        msg1 = bundle.get(EXTRA_1).toString();
        msg2 = bundle.get(EXTRA_2).toString();
        msg3 = bundle.get(EXTRA_3).toString();

        ArticleName=(TextView) findViewById(R.id.nameArticle);
        ArticleStatus=(TextView)findViewById(R.id.statusArticle);
        ArticlePrice=(TextView) findViewById(R.id.priceArticle);
        ImageProduct=(ImageView) findViewById(R.id.imageView18);
        Available =(Button) findViewById(R.id.buttonAddArticle);
        UNAVAILABLE =(Button)findViewById(R.id.buttonPayArticle);
        editText = findViewById(R.id.quantity);
        delete = findViewById(R.id.buttonDelete);
        newprice = findViewById(R.id.newprice);
        buttonPrice = findViewById(R.id.buttonPrice);
        picture = findViewById(R.id.imageView18);

        productService = new ProductService(this, url);
        commandService = new CommandService(this, url);
        productPictureService = new ProductPictureService(this, url);
        productService.getProduct(idProduct, callBackProduct);
        AlertCommand();
        imageView = findViewById(R.id.imageView555);

        buttonPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!newprice.getText().toString().equals("")){
                    available = 3;
                    productService.putProduct(new Product(currentProduct.getId(),null, null,Integer.valueOf(newprice.getText().toString()),0,0,0,0), callBackProductSet);
                }
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_company_page.class);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_NAME, name);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                intent.putExtra(EXTRA_ISCOMMERCANT, 1);
                startActivityForResult(intent,1);
            }
        });

        //TODO: Gérer l'image
        //ImageProduct

        Available.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                available = 0;
                productService.putProduct(new Product(currentProduct.getId(),null, "disponible",0,0,0,0,0), callBackProductSet);
            }
        });

        UNAVAILABLE.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                available = 1;
                productService.putProduct(new Product(currentProduct.getId(),null, "rupture",0,0,0,0,0), callBackProductSet);
            }
        });

        delete.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                available = 2;
                productService.putProduct(new Product(currentProduct.getId(),null, null,0,0,0,0,1), callBackProductSet);
            }
        });
    }

    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            currentProduct = (Product) table.get(0);
            ArticleName.setText(currentProduct.getName());
            ArticleStatus.setText(currentProduct.getStatus());
            ArticlePrice.setText("Price : "+currentProduct.getPrice());
            productPictureService.getProductPicture(-1, callBackProductPocture);
        }


        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductPocture = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t :table
            ) {
                ProductPicture p = (ProductPicture) t;
                if(p.getProductId() == currentProduct.getId()){
                    Picasso.get().load(p.getName()).into(picture);
                }
            }

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductSet = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            if (available == 0)
                Toast.makeText(activity_search_product_commercant.this, "Product available", Toast.LENGTH_SHORT).show();
            else
                if (available == 1)
                    Toast.makeText(activity_search_product_commercant.this, "Product unavailable", Toast.LENGTH_SHORT).show();
                else
                    if (available == 2)
                        Toast.makeText(activity_search_product_commercant.this, "Product deleted", Toast.LENGTH_SHORT).show();
                    else {
                        Toast.makeText(activity_search_product_commercant.this, "Product price changed", Toast.LENGTH_SHORT).show();
                        productService.getProduct(idProduct, callBackProduct);
                    }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    ////////////////////////////////Notification Command///////////////////////////////////////

    private final CallBack callBackCommandNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Command c = (Command) t;
                if(c.getNotif() == 1){
                    if (c.getIdShop() == Integer.valueOf(index1)){
                        createNotificationChannel2();//create notification offer for client
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_search_product_commercant" )
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle(notificationNameCommand)
                                .setContentText(notificationDescriptionCommand)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                        // notificationId is a unique int for each notification that you must define
                        notificationManager.notify(15, builder.build());
                        commandService.putCommand(new Command(c.getId(), null, -1, null, -1, -1, 0, 0),callBackCommandNotifPut );
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandNotifPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

}
