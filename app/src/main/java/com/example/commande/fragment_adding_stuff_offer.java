package com.example.commande;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.Table;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

public class fragment_adding_stuff_offer extends Fragment implements AdapterView.OnItemSelectedListener{

    public static String EXTRA_ID_PRODUCT = "com.example.commande.EXTRA_ID_PRODUCT";
    private ProductService productService;
    private Spinner spinner;
    private Spinner spinner2;
    private List<Product> products = new ArrayList<>();
    private String index1;
    private String index2;
    int idCommercant;
    int idProduct;
    private String url;
    private ArrayList<String> ProductListString=new ArrayList<>();;

    public fragment_adding_stuff_offer() {
        // Required empty public constructor
    }
    public static fragment_adding_stuff_offer newInstance(String param1, String param2) {
        fragment_adding_stuff_offer fragment = new fragment_adding_stuff_offer();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_adding_stuff_offer,
                container, false);
        return view;
    }
    public String getTextToMe()
    {
        return "Mes couilles";
    }

    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
            index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
            url = bundle.get(EXTRA_URL).toString();

            if(index1 != null){
                idCommercant = Integer.valueOf(index1);
            }else{
                idCommercant = Integer.valueOf(index2);
            }
        }

        productService = new ProductService(this.getActivity(), url);
        productService.getProduct(-1, callBackProduct);

        spinner = (Spinner) getView().findViewById(R.id.spinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.offer, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_INDEX_CCOMMERCANT, index1);
        bundle.putString(EXTRA_INDEX_COMMERCANT, index2);
        bundle.putInt(EXTRA_ID_PRODUCT, idProduct);
        bundle.putString(EXTRA_URL, url);

        if(position==0)
        {
            Fragment childFragment = new fragment_adding_stuff_offer_discounts();
            childFragment.setArguments(bundle);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container_view, childFragment).commit();
        }
        else if(position==1)
        {
            Fragment childFragment = new fragment_adding_stuff_offer_for();
            childFragment.setArguments(bundle);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container_view, childFragment).commit();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for(int i=0;i<table.size();i++)
            {
                Product p = (Product) table.get(i);
                if(p.getShopId() == idCommercant){
                    ProductListString.add(p.getName());
                    products.add(p);
                }
            }

            spinner2 = (Spinner) getView().findViewById(R.id.spinner2);

            ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1,ProductListString);
            adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(adapter2);
            spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    idProduct = products.get(i).getId();
                    Bundle bundle = new Bundle();
                    bundle.putString(EXTRA_INDEX_CCOMMERCANT, index1);
                    bundle.putString(EXTRA_INDEX_COMMERCANT, index2);
                    bundle.putInt(EXTRA_ID_PRODUCT, idProduct);
                    bundle.putString(EXTRA_URL, url);
                    int position = spinner.getSelectedItemPosition();

                    if(position==0)
                    {
                        Fragment childFragment = new fragment_adding_stuff_offer_discounts();
                        childFragment.setArguments(bundle);
                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment_container_view, childFragment).commit();
                    }
                    else if(position==1)
                    {
                        Fragment childFragment = new fragment_adding_stuff_offer_for();
                        childFragment.setArguments(bundle);
                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment_container_view, childFragment).commit();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
            Toast.makeText(fragment_adding_stuff_offer.this.getActivity(), "Get product error", Toast.LENGTH_SHORT).show();
        }
    };
}