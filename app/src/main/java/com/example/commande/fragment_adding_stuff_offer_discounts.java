package com.example.commande;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.Table;

import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;
import static com.example.commande.fragment_adding_stuff_offer.EXTRA_ID_PRODUCT;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link fragment_adding_stuff_offer_discounts#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_adding_stuff_offer_discounts extends Fragment {
    int indexCommercant;
    int idProduct;
    private OfferService offerService;
    private ProductService productService;
    private String url;
    private Spinner spinner;
    private Button button;
    private EditText name;
    private EditText valeur1;
    private EditText valeur2;
    private String index1;
    private String index2;
    private int idOffer;


    public fragment_adding_stuff_offer_discounts() {
        // Required empty public constructor
    }

    public static fragment_adding_stuff_offer_discounts newInstance(String param1, String param2) {
        fragment_adding_stuff_offer_discounts fragment = new fragment_adding_stuff_offer_discounts();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_adding_stuff_offer_discounts, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        name = getView().findViewById(R.id.editTextTextPersonName9);
        valeur1 = getView().findViewById(R.id.editTextTextPersonName12);
        button = getView().findViewById(R.id.button11);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
            index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
            idProduct = (int) bundle.get(EXTRA_ID_PRODUCT);
            url = bundle.get(EXTRA_URL).toString();

            if(index1 != null){
                indexCommercant = Integer.valueOf(index1);
            }else{
                indexCommercant = Integer.valueOf(index2);
            }
        }

        offerService = new OfferService(this.getActivity(), url);
        productService = new ProductService(this.getActivity(), url);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productService.getProduct(-1, callBackProductGet);
            }
        });
    }

    private final CallBack callBackOffer2 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t : table
                 ) {
                Offer o = (Offer) t;
                if(o.getId() == idOffer)
                    offerService.putOffer(new Offer(idOffer, name.getText().toString(), 2, Integer.valueOf(valeur1.getText().toString()), 0, indexCommercant, 1), callBackOffer);
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
            Toast.makeText(fragment_adding_stuff_offer_discounts.this.getActivity(), "Adding offer error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackProductGet = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
                 ) {
                Product p = (Product) t;
                if(p.getId() == idProduct)
                    idOffer = p.getOfferId();
            }
            offerService.getOffer(-1, callBackOffer2);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
            Toast.makeText(fragment_adding_stuff_offer_discounts.this.getActivity(), "Adding offer error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackOffer = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productService.putProduct(new Product(idProduct,null, null, -1, table.get(0).getId(), -1, -1, 0), callBackProduct);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
            Toast.makeText(fragment_adding_stuff_offer_discounts.this.getActivity(), "Adding offer error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Toast.makeText(fragment_adding_stuff_offer_discounts.this.getActivity(), "Discount offer added", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };
}