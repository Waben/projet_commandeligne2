package com.example.commande;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.ClientService;
import com.example.commande.service.PasswordService;
import com.example.commande.service.ProductTypeService;
import com.example.commande.service.ShopService;
import com.example.commande.tableClass.Client;
import com.example.commande.tableClass.Password;
import com.example.commande.tableClass.Shop;
import com.example.commande.tableClass.Table;

import java.util.ArrayList;
import java.util.List;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_INDEX_COMMERCANT = "com.example.commande.INDEX_COMMERCANT";
    public static final String EXTRA_INDEX_CLIENT = "com.example.commande.INDEX_CLIENT";
    public static final String EXTRA_URL = "com.example.EXTRA_URL";
    public static final String EXTRA_ISCOMMERCANT = "com.example.EXTRA_ISCOMMERCANT";
    PasswordService passwordService;
    ClientService clientService;
    ShopService shopService;
    List<Table> passwords = new ArrayList<>();
    List<Table> clients = new ArrayList<>();
    List<Table> boutiques = new ArrayList<>();
    EditText password;
    EditText mail;
    private String url = "http://10.135.144.202:5000/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        passwordService = new PasswordService(this, url);
        clientService = new ClientService(this, url);
        shopService = new ShopService(this, url);

        mail = findViewById(R.id.editTextTextPersonName8);
        password = findViewById(R.id.editTextTextPersonName5);

        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {

                passwordService.getPassword(-1, callBackConnexion);
                clientService.getClient(-1,callBackClients );
                shopService.getShop(-1, callBackShop);
            }
        });

        final TextView textView = findViewById(R.id.textView6);
        textView.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), activity_signup.class);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });


    }

    private final CallBack callBackShop = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            boutiques = table;

            if(boutiques.size() != 0 && clients.size() !=0 && passwords.size() != 0 ){
                for (Table t : passwords) {
                    Password p = (Password) t;

                    if (p.getIsCommercant() == 0) {
                        for (Table ta : clients
                        ) {
                            Client c = (Client) ta;
                            if (password.getText().toString().equals(p.getPassword()) && p.getCliend_id() == c.getId() && c.getMail().equals(mail.getText().toString())) {
                                Intent intent = new Intent(getApplicationContext(), activity_client_search.class);
                                intent.putExtra(EXTRA_INDEX_CLIENT, c.getId());
                                intent.putExtra(EXTRA_URL, url);
                                startActivityForResult(intent,1);
                            }
                        }
                    } else if (p.getIsCommercant() == 1) {

                        for (Table ta : boutiques
                        ) {
                            Shop s = (Shop) ta;
                            if (password.getText().toString().equals(p.getPassword()) && p.getCliend_id() == s.getId() && s.getMail().equals(mail.getText().toString())) {
                                Intent intent = new Intent(getApplicationContext(), activity_company_page.class);
                                intent.putExtra(EXTRA_INDEX_COMMERCANT, s.getId());
                                intent.putExtra(EXTRA_ID_TEMPLATE, -1);
                                intent.putExtra(EXTRA_URL, url);
                                intent.putExtra(EXTRA_ISCOMMERCANT, 1);
                                startActivityForResult(intent,1);
                            }
                        }
                    }
                }
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CONNEXION SHOP", exception.toString());
            Toast.makeText(MainActivity.this, "Connexion Client error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackClients = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            clients = table;
            if(boutiques.size() != 0 && clients.size() !=0 && passwords.size() != 0 ){
                for (Table t : passwords) {
                    Password p = (Password) t;

                    if (p.getIsCommercant() == 0) {
                        for (Table ta : clients
                        ) {
                            Client c = (Client) ta;
                            if (password.getText().toString().equals(p.getPassword()) && p.getCliend_id() == c.getId() && c.getMail().equals(mail.getText().toString())) {
                                Intent intent = new Intent(getApplicationContext(), activity_client_search.class);
                                intent.putExtra(EXTRA_INDEX_CLIENT, c.getId());
                                intent.putExtra(EXTRA_URL, url);
                                startActivityForResult(intent,1);
                            }
                        }
                    } else if (p.getIsCommercant() == 1) {

                        for (Table ta : boutiques
                        ) {
                            Shop s = (Shop) ta;
                            if (password.getText().toString().equals(p.getPassword()) && p.getCliend_id() == s.getId() && s.getMail().equals(mail.getText().toString())) {
                                Intent intent = new Intent(getApplicationContext(), activity_company_page.class);
                                intent.putExtra(EXTRA_INDEX_COMMERCANT, s.getId());
                                intent.putExtra(EXTRA_ID_TEMPLATE, -1);
                                intent.putExtra(EXTRA_URL, url);
                                intent.putExtra(EXTRA_ISCOMMERCANT, 1);
                                startActivityForResult(intent,1);
                            }
                        }
                    }
                }
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CONNEXION CLIENT", exception.toString());
            Toast.makeText(MainActivity.this, "Connexion Client error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackConnexion = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            passwords = table;
            if(boutiques.size() != 0 && clients.size() !=0 && passwords.size() != 0 ){
                for (Table t : passwords) {
                    Password p = (Password) t;

                    if (p.getIsCommercant() == 0) {
                        for (Table ta : clients
                        ) {
                            Client c = (Client) ta;
                            if (password.getText().toString().equals(p.getPassword()) && p.getCliend_id() == c.getId() && c.getMail().equals(mail.getText().toString())) {
                                Intent intent = new Intent(getApplicationContext(), activity_client_search.class);
                                intent.putExtra(EXTRA_INDEX_CLIENT, c.getId());
                                intent.putExtra(EXTRA_URL, url);
                                startActivityForResult(intent,1);
                            }
                        }
                    } else if (p.getIsCommercant() == 1) {

                        for (Table ta : boutiques
                        ) {
                            Shop s = (Shop) ta;
                            if (password.getText().toString().equals(p.getPassword()) && p.getCliend_id() == s.getId() && s.getMail().equals(mail.getText().toString())) {
                                Intent intent = new Intent(getApplicationContext(), activity_company_page.class);
                                intent.putExtra(EXTRA_INDEX_COMMERCANT, s.getId());
                                intent.putExtra(EXTRA_ID_TEMPLATE, -1);
                                intent.putExtra(EXTRA_URL, url);
                                intent.putExtra(EXTRA_ISCOMMERCANT, 1);
                                startActivityForResult(intent,1);
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CONNEXION PASSWORD", exception.toString());
            Toast.makeText(MainActivity.this, "Connexion Client error", Toast.LENGTH_SHORT).show();
        }
    };
}