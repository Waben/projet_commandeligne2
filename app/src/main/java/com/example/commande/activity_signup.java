package com.example.commande;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;

public class activity_signup extends AppCompatActivity {

    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        url = (String) bundle.get(EXTRA_URL).toString();

        Bundle bundle2 = new Bundle();
        bundle2.putString(EXTRA_URL, url);

        if(((RadioButton)findViewById(R.id.Sellor)).isChecked())
        {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view, fragment_commercant_connexion.class, bundle2)
                    .commit();
        }
        else
        {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view, fragment_client_connexion.class, bundle2)
                    .commit();
        }
    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_URL, url);

        RadioButton b = (RadioButton) findViewById(view.getId());
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.Sellor:
                if (checked) {
                    for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    }
                    getSupportFragmentManager().beginTransaction()
                            .setReorderingAllowed(true)
                            .add(R.id.fragment_container_view, fragment_commercant_connexion.class, bundle)
                            .commit();
                    break;
                }
            case R.id.Client:
                if (checked){
                    for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    }
                    getSupportFragmentManager().beginTransaction()
                            .setReorderingAllowed(true)
                            .add(R.id.fragment_container_view, fragment_client_connexion.class, bundle)
                            .commit();
                    break;
                }
        }
    }
}