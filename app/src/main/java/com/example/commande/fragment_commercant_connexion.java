package com.example.commande;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.PasswordService;
import com.example.commande.service.ShopService;
import com.example.commande.tableClass.Password;
import com.example.commande.tableClass.Shop;
import com.example.commande.tableClass.Table;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_URL;

public class fragment_commercant_connexion extends Fragment {

    public static final String EXTRA_INDEX_CCOMMERCANT = "com.example.commande.INDEX_COMMERCANT";
    private PasswordService passwordService;
    private ShopService shopService;
    private EditText firstName;
    private EditText lastName;
    private EditText addresse;
    private EditText siret;
    private EditText nameCompagny;
    private EditText mail;
    private EditText password;
    private EditText contact;
    private EditText passwordConfirmation;
    private Button submit;
    private String url;
    private Shop shop;
    private Password passw;
    List<Table> shops = new ArrayList<>();
    boolean used;
    boolean get;
    int indexCommercant;

    public fragment_commercant_connexion() {
        // Required empty public constructor
    }
    public static fragment_commercant_connexion newInstance(String param1, String param2) {
        fragment_commercant_connexion fragment = new fragment_commercant_connexion();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_commercant_connexion, container, false);
    }
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            url = bundle.get(EXTRA_URL).toString();
        }
        firstName = getView().findViewById(R.id.editTextTextPersonName);
        lastName = getView().findViewById(R.id.editTextTextPersonName2);
        addresse = getView().findViewById(R.id.editTextTextPersonName3);
        mail = getView().findViewById(R.id.editTextTextPersonName4);
        nameCompagny = getView().findViewById(R.id.editTextTextPersonName10);
        siret = getView().findViewById(R.id.editTextTextPersonName11);
        password = getView().findViewById(R.id.editTextTextPersonName7);
        submit = getView().findViewById(R.id.button2);
        contact = getView().findViewById(R.id.editTextTextPersonName666);
        passwordConfirmation = getView().findViewById(R.id.editTextTextPersonName555);

        final TextView textView =  getView().findViewById(R.id.textView106);
        textView.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        passwordService = new PasswordService(this.getActivity(), url);
        shopService = new ShopService(this.getActivity(), url);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                used = false;
                get = false;
                shopService.getShop(-1, callBackShop);
            }
        });
    }

    private final CallBack callBackPassword = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Intent intent = new Intent(getContext(), activity_template_choice.class);
            intent.putExtra(EXTRA_INDEX_CCOMMERCANT, indexCommercant);
            intent.putExtra(EXTRA_URL, url);
            startActivityForResult(intent,1);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("INSCRIPTION PASSWORD", exception.toString());
            Toast.makeText(fragment_commercant_connexion.this.getActivity(), "Inscription Client error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackShop = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            shops = table;
            if(!get) {
                get = true;
                for (Table t : shops
                ) {
                    Shop s = (Shop) t;
                    if (s.getMail().equals(mail.getText().toString())) used = true;
                }
                if (!used) {
                    if (password.getText().toString().equals(passwordConfirmation.getText().toString())) {
                        shop = new Shop(-1, nameCompagny.getText().toString(), firstName.getText().toString(), lastName.getText().toString(), siret.getText().toString(), addresse.getText().toString(), mail.getText().toString(), contact.getText().toString());
                        shopService.postShop(shop, callBackShop);
                    }else{
                        Toast.makeText(fragment_commercant_connexion.this.getActivity(), "Wrong password confirmation", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(fragment_commercant_connexion.this.getActivity(), "Mail already used", Toast.LENGTH_SHORT).show();
                }
            }else{
                indexCommercant = shops.get(0).getId();
                passw = new Password(-1, indexCommercant, password.getText().toString(), 1);
                passwordService.postPassword(passw, callBackPassword);
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("INSCRIPTION SHOP", exception.toString());
            Toast.makeText(fragment_commercant_connexion.this.getActivity(), "Inscription Client error", Toast.LENGTH_SHORT).show();
        }
    };
}