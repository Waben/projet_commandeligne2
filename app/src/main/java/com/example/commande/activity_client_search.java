package com.example.commande;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.commande.service.CallBack;
import com.example.commande.service.CategoryService;
import com.example.commande.service.ClientService;
import com.example.commande.service.HobbyListService;
import com.example.commande.service.HobbyService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductPictureService;
import com.example.commande.service.ProductService;
import com.example.commande.service.ShopService;
import com.example.commande.tableClass.Category;
import com.example.commande.tableClass.Client;
import com.example.commande.tableClass.Hobby;
import com.example.commande.tableClass.HobbyList;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.ProductPicture;
import com.example.commande.tableClass.Shop;
import com.example.commande.tableClass.Table;
import com.squareup.picasso.Picasso;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_ISCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

public class activity_client_search extends AppCompatActivity {
    public static String EXTRA_ID_PRODUCT2 = "com.example.commande.EXTRA_ID_PRODUCT2";
    private ShopService shopService;
    private ProductService productService;
    private OfferService offerService;
    private int idClient;
    private String url;
    private ArrayList<Table> TLA=new ArrayList();// arrays of product and shop
    private String search;
    private ListView list;
    private TextView Offre;
    private Product currentProdutc;
    private List<Offer> offers;
    private Spinner spinner;
    private CategoryService categoryService;
    private HobbyService hobbyService;
    private HobbyListService hobbyListService;
    private List<Category> categories;
    private List<Hobby> hobbies;
    private List<HobbyList> hobbyLists;
    private ClientService clientService;
    private int idHobby;
    private ImageView picture;
    private ProductPictureService productPictureService;
    private  List<ProductPicture> productPictures;
    private List<Product> productsNotif;
    private String notificationName = "New Offer !";
    private String notificationDescription = "There is a new offer for the shop : ";

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_client_search", notificationName, importance);
            channel.setDescription(notificationDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @MainThread
    private void Alert(){
        productService.getProduct(-1, callBackProductNotif);
    }

    class TableList  extends BaseAdapter {
        List<Table> dataSource;

        public TableList(List<Table> data) {
            //super( activity_client_search.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Table> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateTableList(List<Table> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Table c = dataSource.get(position);
            if(c instanceof Product)
            {
                currentProdutc = (Product)c;
                convertView = LayoutInflater.from(activity_client_search.this).inflate(R.layout.search_row_listview_table_product, null);
                TextView Name = (TextView) convertView.findViewById(R.id.Nom);
                TextView Price = (TextView) convertView.findViewById(R.id.Price);
                TextView Statut = (TextView) convertView.findViewById(R.id.Statut);
                picture = convertView.findViewById(R.id.imageView18);
                Offre = (TextView) convertView.findViewById(R.id.Offre);

                Name.setText(((Product) c).getName());
                Price.setText(((Product) c).getPrice()+"$");
                Statut.setText(((Product) c).getStatus());

                for (ProductPicture p :productPictures
                     ) {
                    if (p.getProductId() == c.getId()){
                        Picasso.get().load(p.getName()).into(picture);
                    }
                }

                for (Offer o: offers
                     ) {
                    if(o.getId() == currentProdutc.getOfferId()){
                        if(o.getOfferType() == 0){
                            Offre.setText("No offer");
                        }
                        if (o.getOfferType() == 1){
                            Offre.setText("For "+ o.getValue1() + " get " + o.getValue2());
                        }
                        if (o.getOfferType() == 2){
                            Offre.setText("-" + o.getValue1()+"%");
                        }
                        break;
                    }
                }



            }
            else if(c instanceof Shop)
            {
                convertView = LayoutInflater.from(activity_client_search.this).inflate(R.layout.search_row_listview_table_company, null);
                TextView Name = (TextView) convertView.findViewById(R.id.Nom);
                TextView Address = (TextView) convertView.findViewById(R.id.Address);
                TextView Mail = (TextView) convertView.findViewById(R.id.mail);
                TextView Contact = (TextView) convertView.findViewById(R.id.Contact);

                Name.setText(((Shop) c).getShopName());
                Address.setText(((Shop) c).getAddresse()+"");
                Mail.setText(((Shop) c).getMail());
                Contact.setText(((Shop) c).getContact());

            }
            return convertView;
        }
    }
    TableList TL=null;




    public static final String TAG = com.example.commande.tableClass.Command.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_search);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        url = bundle.get(EXTRA_URL).toString();
        idClient = (int) bundle.get(EXTRA_INDEX_CLIENT);

        productService = new ProductService(this, url);
        offerService = new OfferService(this, url);
        shopService = new ShopService(this, url);
        categoryService = new CategoryService(this, url);
        hobbyListService = new HobbyListService(this, url);
        hobbyService = new HobbyService(this, url);
        clientService = new ClientService(this, url);
        productPictureService = new ProductPictureService(this, url);
        offerService.getOffer(-1,callBackOffer2 );
        Alert();

        list = (ListView) findViewById(R.id.listview);
        spinner = findViewById(R.id.spinner4);
        ImageView chariot = (ImageView) findViewById(R.id.imageView16);
        ImageView personne = (ImageView) findViewById(R.id.imageView17);


        chariot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_panier.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });
        personne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_account_client.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        EditText field1 = (EditText)findViewById(R.id.editTextTextPersonName18);
        field1.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0) {
                    for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    }
                    search = s.toString();
                    if (spinner.getSelectedItemPosition() == 0) {
                        //Products Name
                        offerService.getOffer(-1, callBackOffer4);

                    }
                    if (spinner.getSelectedItemPosition() == 1 ){
                        //Categories
                        categoryService.getCategory(-1, callBackCategories);
                    }

                    if (spinner.getSelectedItemPosition() ==2){
                        //hobbies
                        clientService.getClient(idClient, callBackClient);
                    }
                    if(spinner.getSelectedItemPosition() ==3){
                        //shops
                        shopService.getShop(-1, callBackshop2);
                    }
                    if (spinner.getSelectedItemPosition() ==4){
                        //offers
                        offerService.getOffer(-1,callBackOffer);
                    }
                }else{
                    productService.getProduct(-1, callBackProduct);
                }
            }
        });
    }
    private final CallBack callBackPicture = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productPictures = new ArrayList<>();
            for (Table t: table
            ) {
                ProductPicture p = (ProductPicture) t;
                productPictures.add(p);
            }

            TL=new TableList(TLA);
            list.setAdapter(TL);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView parent, View v, int position, long id)
                {
                    if(TL.getItem(position) instanceof Product) {
                        Product pro = (Product)TL.getItem(position);
                        Intent intent = new Intent(getApplicationContext(), activity_search_product.class);
                        intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                        intent.putExtra(EXTRA_ID_PRODUCT2, pro.getId());
                        intent.putExtra(EXTRA_URL, url);
                        startActivityForResult(intent,1);
                    }
                    else if (TL.getItem(position) instanceof Shop)
                    {
                        Shop s = (Shop) TL.getItem(position);
                        Intent intent = new Intent(getApplicationContext(), activity_company_page.class);
                        intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                        intent.putExtra(EXTRA_INDEX_CCOMMERCANT, s.getId());
                        intent.putExtra(EXTRA_URL, url);
                        intent.putExtra(EXTRA_INDEX_COMMERCANT,s.getId());
                        intent.putExtra(EXTRA_ISCOMMERCANT, 0);
                        startActivityForResult(intent,1);
                    }

                }
            });

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    /////////////////////////////////research hobbies///////////////////////////////////////

    private final CallBack callBackHobby = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Hobby h = (Hobby) t;
                for (HobbyList ho: hobbyLists
                     ) {
                    if (ho.getHobbyId() == h.getId())
                        hobbies.add(h);
                }

            }
            categoryService.getCategory(-1,callBackCategory );
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
        }
    };

    private final CallBack callBackClient = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            offers = new ArrayList<>();
            categories = new ArrayList<>();
            hobbies = new ArrayList<>();
            hobbyLists = new ArrayList<>();
            TLA = new ArrayList<>();
            Client c = (Client) table.get(0);
            idHobby = c.getIdHobby();
            hobbyListService.getHobbyList(-1, callBackHobbyList);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
        }
    };

    private final CallBack callBackHobbyList = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                HobbyList h = (HobbyList) t;
                if (h.getIdHobby() == idHobby)
                    hobbyLists.add(h);
            }
            hobbyService.getHobby(-1, callBackHobby);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
        }
    };

    private final CallBack callBackCategory = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Category c = (Category) t;
                for (Hobby h:hobbies
                     ) {
                    if(c.getName().equals(h.getName()))
                        categories.add(c);
                }
            }
            productService.getProduct(-1,callBackProduct9 );
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
        }
    };

    private final CallBack callBackProduct9 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Product p = (Product) t;
                for (Category c:categories
                     ) {
                    if (p.getName().startsWith(search) && p.getCategoryId() == c.getId() && p.getIsDeleted() ==0)
                        TLA.add(p);
                }
            }
            productPictureService.getProductPicture(-1, callBackPicture);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    //////////////////////////////////research product name/////////////////////////////////
    private final CallBack callBackOffer4 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            offers = new ArrayList<>();
            TLA = new ArrayList<>();
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                offers.add(o);
            }
            productService.getProduct(-1, callBackProduct4);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
        }
    };

    private final CallBack callBackProduct4 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Product p = (Product) t;
                if (p.getName().startsWith(search)&& p.getIsDeleted() ==0)
                    TLA.add(p);


            }

            productPictureService.getProductPicture(-1, callBackPicture);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
    //////////////////////////////////research categories///////////////////////////////////
    private final CallBack callBackCategories = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            offers = new ArrayList<>();
            categories = new ArrayList<>();
            TLA = new ArrayList<>();
            for (Table t: table
            ) {
                Category c = (Category) t;
                if (c.getName().startsWith(search))
                    categories.add(c);
            }
            offerService.getOffer(-1,callBackOffer6 );
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
        }
    };

    private final CallBack callBackOffer6 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                offers.add(o);
            }
            productService.getProduct(-1, callBackProduct6);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
        }
    };

    private final CallBack callBackProduct6 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Product p = (Product) t;
                for (Category c: categories
                ) {
                    if (p.getCategoryId() == c.getId()&& p.getIsDeleted() ==0)
                        TLA.add(p);
                }

            }

            productPictureService.getProductPicture(-1, callBackPicture);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
    //////////////////////////////////research offers///////////////////////////////////////
    private final CallBack callBackOffer = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            offers = new ArrayList<>();
            TLA = new ArrayList<>();
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getOfferType() !=0)
                    offers.add(o);
            }
            productService.getProduct(-1, callBackProduct3);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
        }
    };

    private final CallBack callBackProduct3 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Product p = (Product) t;
                for (Offer o: offers
                     ) {
                    if (p.getOfferId() == o.getId() && p.getName().startsWith(search)&& p.getIsDeleted() ==0)
                        TLA.add(p);
                }

            }

            productPictureService.getProductPicture(-1, callBackPicture);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    ////////////////////////////////////////Research shop ////////////////////////////////////////////////////////
    private final CallBack callBackshop2 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            TLA = new ArrayList<>();
            for (Table t: table
            ) {
                Shop o = (Shop) t;
                if(o.getShopName().startsWith(search))
                    TLA.add(o);
            }

            offerService.getOffer(-1,callBackOffer3 );
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("Product 2 ERROR", exception.toString());
        }
    };

    private final CallBack callBackOffer3 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            offers = new ArrayList<>();
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                offers.add(o);
            }
            productPictureService.getProductPicture(-1, callBackPicture);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    ///////////////////////////////////General research////////////////////////////////////////////////////:
    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
           TLA = new ArrayList<>();
            for (Table t: table
            ) {
                Product o = (Product) t;
                if(o.getIsDeleted() ==0)
                    TLA.add(o);
            }
            shopService.getShop(-1, callBackshop);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackshop = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Shop o = (Shop) t;
                TLA.add(o);
            }

            productPictureService.getProductPicture(-1, callBackPicture);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOffer2 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            offers = new ArrayList<>();
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                offers.add(o);
            }
            productService.getProduct(-1, callBackProduct);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    /////////////////////////////Notification//////////////////////////////////////
    private final CallBack callBackProductNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
           productsNotif = new ArrayList<>();
            for (Table t: table
            ) {
                Product o = (Product) t;
                productsNotif.add(o);

            }
            offerService.getOffer(-1,callBackOfferNotif );

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductOfferPut= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOfferNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
           for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getNotif() == 1){
                    for (Product p : productsNotif
                         ) {
                        if (o.getId() == p.getOfferId()){
                            notificationDescription += p.getName();
                            createNotificationChannel();//create notification offer for client
                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_client_search" )
                                    .setSmallIcon(R.drawable.ic_launcher_background)
                                    .setContentTitle(notificationName)
                                    .setContentText(notificationDescription)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                            // notificationId is a unique int for each notification that you must define
                            notificationManager.notify(1, builder.build());
                            offerService.putOffer(new Offer(o.getId(), null, -1, -1,-1,-1,0),callBackProductOfferPut );
                        }
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

}