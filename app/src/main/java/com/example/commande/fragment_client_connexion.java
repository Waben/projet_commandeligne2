package com.example.commande;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.ClientService;
import com.example.commande.service.PasswordService;
import com.example.commande.tableClass.Client;
import com.example.commande.tableClass.Password;
import com.example.commande.tableClass.Table;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_URL;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link fragment_client_connexion#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_client_connexion extends Fragment {

    public static final String EXTRA_INDEX_CLIENT = "com.example.commande.INDEX_CLIENT";
    private EditText mail;
    private EditText password;
    private EditText passwordConfirmation;
    private EditText addresse;
    private EditText firstName;
    private EditText lastName;
    private PasswordService passwordService;
    private ClientService clientService;
    private String url;
    Client client;
    Password passw;
    private Button submit;
    boolean get;
    int indexClient;
    private Client c;


    List<Table> passwords = new ArrayList<>();
    List<Table> clients = new ArrayList<>();
    public fragment_client_connexion() {
        // Required empty public constructor
    }
    public static fragment_client_connexion newInstance(String param1, String param2) {
        fragment_client_connexion fragment = new fragment_client_connexion();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_client_connexion, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            url = bundle.get(EXTRA_URL).toString();
        }
        final TextView textView =  getView().findViewById(R.id.textView106);
        firstName = getView().findViewById(R.id.editTextTextPersonName);
        lastName = getView().findViewById(R.id.editTextTextPersonName2);
        addresse = getView().findViewById(R.id.editTextTextPersonName3);
        mail = getView().findViewById(R.id.editTextTextPersonName4);
        password = getView().findViewById(R.id.editTextTextPersonName10);
        passwordConfirmation = getView().findViewById(R.id.editTextTextPersonName11);
        submit = getView().findViewById(R.id.button23); //MODIFIER 2-> 23 on sait jamais
        textView.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        passwordService = new PasswordService(this.getActivity(), url);
        clientService = new ClientService(this.getActivity(), url);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clientService.getClient(-1, callBackClients);
                get = false;
            }
        });
    }
    private final CallBack callBackClients = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            clients = table;

            if(!get) {
                get = true;
                boolean used = false;
                for (Table t : clients
                ) {
                    Client c = (Client) t;
                    if (c.getMail().equals(mail.getText().toString())) used = true;
                }
                if (!used) {
                    if (password.getText().toString().equals(passwordConfirmation.getText().toString())) {
                        client = new Client(-1, firstName.getText().toString(), lastName.getText().toString(), mail.getText().toString(), addresse.getText().toString(), 0, 0); //panier et centre interet vide au debut
                        clientService.postClient(client, callBackClients);
                    }else {
                        Toast.makeText(fragment_client_connexion.this.getActivity(), "Wrong password confirmation", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(fragment_client_connexion.this.getActivity(), "Mail already used", Toast.LENGTH_SHORT).show();
                }
            }else{
                c =(Client) clients.get(0);
                indexClient = clients.get(0).getId();
                passw = new Password(-1, indexClient, password.getText().toString(), 0);
                passwordService.postPassword(passw, callBackConnexion);
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("INSCRIPTION CLIENT", exception.toString());
            Toast.makeText(fragment_client_connexion.this.getActivity(), "Inscription Client error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackConnexion = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            c.setIdHobby(c.getId());
            c.setIdBasket(c.getId());
            clientService.putClient(c,callBackConnexion2 );
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("INSCRIPTION PASSWORD", exception.toString());
            Toast.makeText(fragment_client_connexion.this.getActivity(), "Inscription Client error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackConnexion2 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Intent intent = new Intent(getContext(), activity_client_search.class);
            intent.putExtra(EXTRA_INDEX_CLIENT, indexClient);
            intent.putExtra(EXTRA_URL, url);
            startActivityForResult(intent,1);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("INSCRIPTION PASSWORD", exception.toString());
            Toast.makeText(fragment_client_connexion.this.getActivity(), "Inscription Client error", Toast.LENGTH_SHORT).show();
        }
    };
}
