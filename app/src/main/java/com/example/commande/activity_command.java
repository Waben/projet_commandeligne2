package com.example.commande;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.ClientService;
import com.example.commande.service.CommandListService;
import com.example.commande.service.CommandService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Client;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.CommandList;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.Table;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_configure_template.EXTRA_1;
import static com.example.commande.activity_configure_template.EXTRA_2;
import static com.example.commande.activity_configure_template.EXTRA_3;
import static com.example.commande.activity_configure_template.EXTRA_NAME;
import static com.example.commande.activity_orders_list.EXTRA_ID_COMMAND;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

public class activity_command extends AppCompatActivity {

    private ClientService clientService;
    private CommandService commandService;
    private CommandListService commandListService;
    private ProductService productService;
    private List<Integer> indexProduct;
    TextView commandName;
    TextView lastName;
    TextView firstName;
    TextView address;
    TextView price;
    TextView status;
    private ListView list;
    private int idClient;
    private String url;
    int idCommand;
    String msg1;
    String msg2;
    String msg3;
    String name;
    int idTemplate;
    String index1;
    String index2;
    private ImageView imageView;
    private String notificationNameCommand = "New Command !";
    private String notificationDescriptionCommand = "There is a new command for your shop !";

    private void createNotificationChannel2() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_command", notificationNameCommand, importance);
            channel.setDescription(notificationDescriptionCommand);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void AlertCommand(){
        commandService.getCommand(-1, callBackCommandNotif);
    }

    class ProductList  extends BaseAdapter {
        List<Product> dataSource;

        public ProductList(List<Product> data) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Product> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateCityList(List<Product> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Product c = dataSource.get(position);
            convertView = LayoutInflater.from(activity_command.this).inflate(R.layout.activity_command_item_product,null);

            TextView Text1 = (TextView) convertView.findViewById(R.id.textView5);

            Text1.setText("Product "+position + " " + c.getName());


            return convertView;
        }
    }
    ProductList PL=null;


    private static final String TAG = com.example.commande.tableClass.Command.class.getSimpleName();
    Command command;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_command);

        imageView = findViewById(R.id.imageView80);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        idCommand = (int) bundle.get(EXTRA_ID_COMMAND);
        url = bundle.get(EXTRA_URL).toString();
        index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
        index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
        name = bundle.get(EXTRA_NAME).toString();
        msg1 = bundle.get(EXTRA_1).toString();
        msg2 = bundle.get(EXTRA_2).toString();
        msg3 = bundle.get(EXTRA_3).toString();
        idTemplate = (int) bundle.get(EXTRA_ID_TEMPLATE);


        clientService = new ClientService(this, url);
        commandService = new CommandService(this, url);
        productService = new ProductService(this, url);
        commandListService = new CommandListService(this, url);
        AlertCommand();

        command = (Command) getIntent().getParcelableExtra(activity_command.TAG);
        commandName = (TextView) findViewById(R.id.textView12);
        lastName = (TextView) findViewById(R.id.textView13);
        firstName = (TextView) findViewById(R.id.textView14);
        address = (TextView) findViewById(R.id.textView21);
        price = (TextView) findViewById(R.id.textView20);
        status = (TextView) findViewById(R.id.textView19);
        list = (ListView) findViewById(R.id.listview);

        commandName.setText("Command : " + String.valueOf(idCommand));
        commandService.getCommand(idCommand, callBackCommand);
        commandListService.getCommandList(-1,callBackCommandList );

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_orders_list.class);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_NAME, name);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                startActivityForResult(intent,1);
            }
        });

    }

    private final CallBack callBackProduct= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            ArrayList<Product> ArrOff=new ArrayList<>();

            for (int i: indexProduct
                 ) {
                Product p = (Product) table.get(i);
                ArrOff.add(p);
            }
            PL=new activity_command.ProductList(ArrOff);
            list.setAdapter(PL);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommand= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Command c  =(Command) table.get(0);
            price.setText(String.valueOf(c.getPrice()));
            status.setText(c.getStatus());
            idClient = c.getIdClient();
            clientService.getClient(idClient, callBackClient);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandList= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            indexProduct = new ArrayList<>();
            for (Table t: table
                 ) {
                CommandList commandList = (CommandList) t;
                if(commandList.getCommandId() == idCommand){
                    indexProduct.add(commandList.getProductId());
                }
            }
            productService.getProduct(-1, callBackProduct);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMANDLIST ERROR", exception.toString());
        }
    };

    private final CallBack callBackClient= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Client c = (Client) table.get(0);
            firstName.setText(c.getName());
            lastName.setText(c.getSurname());
            address.setText(c.getAddresse());
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("Client ERROR", exception.toString());
        }
    };

    ////////////////////////////////Notification Command///////////////////////////////////////

    private final CallBack callBackCommandNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Command c = (Command) t;
                if(c.getNotif() == 1){
                    if (c.getIdShop() == Integer.valueOf(index1)){
                        createNotificationChannel2();//create notification offer for client
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_command" )
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle(notificationNameCommand)
                                .setContentText(notificationDescriptionCommand)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                        // notificationId is a unique int for each notification that you must define
                        notificationManager.notify(12, builder.build());
                        commandService.putCommand(new Command(c.getId(), null, -1, null, -1, -1, 0, 0),callBackCommandNotifPut );
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandNotifPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}
