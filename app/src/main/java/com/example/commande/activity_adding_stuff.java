package com.example.commande;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.Table;

import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_ISCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_configure_template.EXTRA_1;
import static com.example.commande.activity_configure_template.EXTRA_2;
import static com.example.commande.activity_configure_template.EXTRA_3;
import static com.example.commande.activity_offers_list.EXTRA_POSITION;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;
import static com.example.commande.activity_configure_template.EXTRA_NAME;

public class activity_adding_stuff extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    int SpinnerPosition=0;
    String index1;
    String index2;
    String url;
    String name;
    String msg1;
    String msg2;
    String msg3;
    int idTemplate;
    private CommandService commandService;
    private String notificationNameCommand = "New Command !";
    private String notificationDescriptionCommand = "There is a new command for your shop !";

    private void createNotificationChannel2() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_adding_stuff", notificationNameCommand, importance);
            channel.setDescription(notificationDescriptionCommand);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void AlertCommand(){
        commandService.getCommand(-1, callBackCommandNotif);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_stuff);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
        index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
        url = bundle.get(EXTRA_URL).toString();
        name = bundle.get(EXTRA_NAME).toString();
        msg1 = bundle.get(EXTRA_1).toString();
        msg2 = bundle.get(EXTRA_2).toString();
        msg3 = bundle.get(EXTRA_3).toString();
        idTemplate = (int) bundle.get(EXTRA_ID_TEMPLATE);
        commandService = new CommandService(this, url);
        AlertCommand();

        ImageView imageView = findViewById(R.id.imageView777);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.stuff, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_company_page.class);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_NAME, name);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                intent.putExtra(EXTRA_ISCOMMERCANT, 1);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                startActivityForResult(intent,1);
            }
        });

        ImageView b = (ImageView) findViewById(R.id.imageView2);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String text = spinner.getSelectedItem().toString();
                if(SpinnerPosition==0)
                {
                    FragmentManager fm = getSupportFragmentManager();
                    //TODO: PRODUCT FRAGMENT
                    //fragment_adding_stuff_product fragment = (fragment_adding_stuff_product)fm.findFragmentById(R.id.fragment_container_view);


                    Toast.makeText(activity_adding_stuff.this,"Product",Toast.LENGTH_LONG).show();
                }
                else if(SpinnerPosition==1)
                {
                    FragmentManager fm = getSupportFragmentManager();
                    fragment_adding_stuff_categories fragment = (fragment_adding_stuff_categories)fm.findFragmentById(R.id.fragment_container_view);

                    Toast.makeText(activity_adding_stuff.this,"Categories",Toast.LENGTH_LONG).show();
                }
                else if(SpinnerPosition==2)
                {
                    FragmentManager fm = getSupportFragmentManager();
                    fragment_adding_stuff_offer fragment = (fragment_adding_stuff_offer)fm.findFragmentById(R.id.fragment_container_view);
                    //fragment.getTextToMe()

                    Toast.makeText(activity_adding_stuff.this,"Offer",Toast.LENGTH_LONG).show();
                }
            }
        });



    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_INDEX_CCOMMERCANT, index1);
        bundle.putString(EXTRA_INDEX_COMMERCANT, index2);
        bundle.putString(EXTRA_URL, url);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }

        SpinnerPosition=position;
        if(position==0)
        {
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view, fragment_adding_stuff_product.class, bundle)
                    .commit();

        }
        else if(position==2)
        {
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view, fragment_adding_stuff_offer.class, bundle)
                    .commit();
        }
        else if(position==1)
        {
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view, fragment_adding_stuff_categories.class, bundle)
                    .commit();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    ////////////////////////////////Notification Command///////////////////////////////////////

    private final CallBack callBackCommandNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Command c = (Command) t;
                if(c.getNotif() == 1){
                    if (c.getIdShop() == Integer.valueOf(index1)){
                        createNotificationChannel2();//create notification offer for client
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_adding_stuff" )
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle(notificationNameCommand)
                                .setContentText(notificationDescriptionCommand)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                        // notificationId is a unique int for each notification that you must define
                        notificationManager.notify(10, builder.build());
                        commandService.putCommand(new Command(c.getId(), null, -1, null, -1, -1, 0, 0),callBackCommandNotifPut );
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandNotifPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}
