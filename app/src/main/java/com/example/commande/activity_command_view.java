package com.example.commande;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandListService;
import com.example.commande.service.CommandService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductPictureService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.CommandList;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.ProductPicture;
import com.example.commande.tableClass.Table;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_panier.EXTRA_IDCOMMAND;

public class activity_command_view extends AppCompatActivity {
    class CommandListView extends BaseAdapter {
        List<Product> dataSource;

        public CommandListView(List<Product> data) {
            //super( activity_client_search.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Product> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateTableList(List<Product> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Product c = dataSource.get(position);


            convertView = LayoutInflater.from(activity_command_view.this).inflate(R.layout.row_command_view, null);

            TextView Name = (TextView) convertView.findViewById(R.id.textView28);
            TextView Price = (TextView) convertView.findViewById(R.id.textView30);
            ImageView picture = convertView.findViewById(R.id.imageView23);

            for (ProductPicture p:productPictures
                 ) {
                if (c.getId() == p.getProductId()){
                    Picasso.get().load(p.getName()).into(picture);
                }
            }

            Name.setText(c.getName());
            for (com.example.commande.tableClass.CommandList a: commandLists
            ) {
                if (a.getProductId()==c.getId())
                    Price.setText(c.getPrice()+"$ x"+(a.getQuantity()+a.getQuantityOffer()));
            }

            return convertView;
        }
    }
    CommandListView PL=null;
    private String url;
    private int idClient;
    private int idCommand;
    private ImageView personne;
    private  ImageView chariot;
    private ImageView retour;
    private int prix;
    private ListView list;
    private TextView prixtotal;
    private CommandService commandService;
    private CommandListService commandListService;
    private ProductService productService;
    private List<Product> products;
    private List<CommandList> commandLists;
    private Command currentCommand;
    private ProductPictureService productPictureService;
    private List<ProductPicture> productPictures;
    private OfferService offerService;
    private List<Product> productsNotif;
    private String notificationName = "New Offer !";
    private String notificationDescription = "There is a new offer for the shop : ";

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_command_view", notificationName, importance);
            channel.setDescription(notificationDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void Alert(){
        productService.getProduct(-1, callBackProductNotif);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_command_view);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        personne = findViewById(R.id.imageView22);
        chariot = findViewById(R.id.imageView21);
        retour = findViewById(R.id.imageView777);
        list = findViewById(R.id.LISTVIEW2);

        url = bundle.get(EXTRA_URL).toString();
        idClient =(int) bundle.get(EXTRA_INDEX_CLIENT);
        idCommand = (int) bundle.get(EXTRA_IDCOMMAND);
        prixtotal = (TextView) findViewById(R.id.textView29);
        commandService = new CommandService(this, url);
        commandListService = new CommandListService(this, url);
        productService = new ProductService(this, url);
        offerService = new OfferService(this, url);
        productPictureService = new ProductPictureService(this, url);
        Alert();
        productPictureService.getProductPicture(-1, callBackProductPicture);

        chariot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_panier.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_command_list.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        personne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_account_client.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });
    }

    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
             for (Table t:table
            ) {
                Product p = (Product) t;
                for (CommandList c: commandLists
                ) {
                    if(c.getProductId() == p.getId()){
                        products.add(p);
                    }
                }
            }
            PL=new CommandListView(products);
            list.setAdapter(PL);
            setTotalPrice();
        }


        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductPicture = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productPictures = new ArrayList<>();

            for (Table t : table
            ) {
                ProductPicture c = (ProductPicture) t;
                productPictures.add(c);
            }
            commandService.getCommand(idCommand,callBackCommand );
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("callBackCommandList ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandList = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t : table
            ) {
                CommandList c = (CommandList)t;
                if(c.getCommandId() == currentCommand.getId()){
                    commandLists.add(c);
                }


            }
            productService.getProduct(-1, callBackProduct);
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("callBackCommandList ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommand = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            products = new ArrayList<>();
            currentCommand = (Command) table.get(0);
            commandLists = new ArrayList<>();
            commandListService.getCommandList(-1, callBackCommandList);
        }


        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND ERROR", exception.toString());
        }
    };

    private void setTotalPrice(){
        prix=0;
        for (CommandList c:commandLists
        ) {
            for (Product p:products
            ) {
                if (c.getProductId() == p.getId()){
                    prix+=p.getPrice()*c.getQuantity();
                }
            }

        }
        prixtotal.setText("Total : "+prix);
    }

    /////////////////////////////Notification//////////////////////////////////////
    private final CallBack callBackProductNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productsNotif = new ArrayList<>();
            for (Table t: table
            ) {
                Product o = (Product) t;
                productsNotif.add(o);

            }
            offerService.getOffer(-1,callBackOfferNotif );

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductOfferPut= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOfferNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getNotif() == 1){
                    for (Product p : productsNotif
                    ) {
                        if (o.getId() == p.getOfferId()){
                            notificationDescription += p.getName();
                            createNotificationChannel();//create notification offer for client
                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_command_view" )
                                    .setSmallIcon(R.drawable.ic_launcher_background)
                                    .setContentTitle(notificationName)
                                    .setContentText(notificationDescription)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                            // notificationId is a unique int for each notification that you must define
                            notificationManager.notify(4, builder.build());
                            offerService.putOffer(new Offer(o.getId(), null, -1, -1,-1,-1,0),callBackProductOfferPut );
                        }
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}
