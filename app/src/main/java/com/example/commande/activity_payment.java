package com.example.commande;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.CommandList;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.Table;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_panier.EXTRA_IDCOMMAND;

public class activity_payment extends AppCompatActivity {

    private int idClient;
    private String url;
    private CommandService commandService;
    private ImageView command;
    private Spinner spinner;
    private OfferService offerService;
    private ProductService productService;
    private List<Product> productsNotif;
    private String notificationName = "New Offer !";
    private String notificationDescription = "There is a new offer for the shop : ";

    private void Alert(){
        productService.getProduct(-1, callBackProductNotif);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_payment", notificationName, importance);
            channel.setDescription(notificationDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        idClient = (int) bundle.get(EXTRA_INDEX_CLIENT);
        url = bundle.get(EXTRA_URL).toString();
        commandService = new CommandService(this, url);
        productService = new ProductService(this, url);
        offerService = new OfferService(this, url);
        Alert();

        EditText deliveryA=(EditText) findViewById(R.id.editTextTextPersonName919);
        EditText bilingA=(EditText) findViewById(R.id.editTextTextPersonName922);
        EditText cb=(EditText) findViewById(R.id.editTextTextPersonName923);
        EditText crypto=(EditText) findViewById(R.id.editTextTextPersonName924);
        spinner = findViewById(R.id.spinner5);
        command = findViewById(R.id.imageView555);

        ImageView chariot = (ImageView) findViewById(R.id.imageView19);
        ImageView personne = (ImageView) findViewById(R.id.imageView20);


        Button Pay = (Button) findViewById(R.id.button9);

        command.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_panier.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        Pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String d = deliveryA.getText().toString();
                String b = bilingA.getText().toString();
                String cb2 = cb.getText().toString();
                String c = crypto.getText().toString();
                if (!(d.equals("") || b.equals("") || cb2.equals("") || c.equals("")))
                    commandService.getCommand(-1,callBackCommandGet );
               }
        });
        chariot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_panier.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });
        personne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_account_client.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });
    }

    private final CallBack callBackCommandGet = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t:table
                 ) {
                Command c = (Command)t;
                if(c.getIdClient() == idClient && c.getStatus().equals("disponible")){
                    commandService.putCommand(new Command(c.getId(), java.time.LocalDate.now().toString(), 0, "en cours de livraison", 0, 0, 1, spinner.getSelectedItemPosition() +1),callBackCommand );
                }
            }
            Toast.makeText(activity_payment.this, "Command in progress", Toast.LENGTH_SHORT).show();
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("callBackCommandList ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommand = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {

        }

        @SuppressLint("LongLogTag")
        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("callBackCommandList ERROR", exception.toString());
        }
    };

    /////////////////////////////Notification//////////////////////////////////////
    private final CallBack callBackProductNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productsNotif = new ArrayList<>();
            for (Table t: table
            ) {
                Product o = (Product) t;
                productsNotif.add(o);

            }
            offerService.getOffer(-1,callBackOfferNotif );

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductOfferPut= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOfferNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getNotif() == 1){
                    for (Product p : productsNotif
                    ) {
                        if (o.getId() == p.getOfferId()){
                            notificationDescription += p.getName();
                            createNotificationChannel();//create notification offer for client
                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_payment" )
                                    .setSmallIcon(R.drawable.ic_launcher_background)
                                    .setContentTitle(notificationName)
                                    .setContentText(notificationDescription)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                            // notificationId is a unique int for each notification that you must define
                            notificationManager.notify(6, builder.build());
                            offerService.putOffer(new Offer(o.getId(), null, -1, -1,-1,-1,0),callBackProductOfferPut );
                        }
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}