package com.example.commande;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.CategoryService;
import com.example.commande.service.HobbyService;
import com.example.commande.tableClass.Category;
import com.example.commande.tableClass.Hobby;
import com.example.commande.tableClass.Table;

import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_orders_list.EXTRA_ID_COMMAND;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link fragment_adding_stuff_categories#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_adding_stuff_categories extends Fragment {

    private CategoryService categoryService;
    private HobbyService hobbyService;
    private EditText name;
    private Button button;
    private String url;

    public fragment_adding_stuff_categories() {
        // Required empty public constructor
    }

    public static fragment_adding_stuff_categories newInstance(String param1, String param2) {
        fragment_adding_stuff_categories fragment = new fragment_adding_stuff_categories();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_adding_stuff_categories, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        name = getView().findViewById(R.id.editTextTextPersonName16);
        button = getView().findViewById(R.id.button13);

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            url = bundle.get(EXTRA_URL).toString();
        }
        categoryService = new CategoryService(this.getActivity(), url);
        hobbyService = new HobbyService(this.getActivity(), url);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryService.postCategory(new Category(-1, name.getText().toString()),callBackCategory );
            }
        });
    }

    private final CallBack callBackHobby = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Toast.makeText(fragment_adding_stuff_categories.this.getActivity(), "Category added", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackCategory = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Category c = (Category)table.get(0);
            hobbyService.postHobby(new Hobby(-1,c.getName()), callBackHobby);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}
