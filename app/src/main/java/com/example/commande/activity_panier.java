package com.example.commande;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandListService;
import com.example.commande.service.CommandService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductPictureService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.CommandList;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.ProductPicture;
import com.example.commande.tableClass.Shop;
import com.example.commande.tableClass.Table;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_URL;

public class activity_panier extends AppCompatActivity {

    public static String EXTRA_IDCOMMAND = "com.example.command.EXTRA_IDCOMMAND";
    private String url;
    private int idClient;
    private CommandService commandService;
    private CommandListService commandListService;
    private ProductService productService;
    private int prix;
    private List<CommandList> commandLists;
    private List<Product> products;
    private ListView list;
    private  TextView prixtotal;
    private List<Command> currentCommand;
    private Product deleteProduct;
    private Boolean childs;
    private CommandList delete;
    private Offer deleteOffer;
    private OfferService offerService;
    private List<Product> productsNotif;
    private String notificationName = "New Offer !";
    private String notificationDescription = "There is a new offer for the shop : ";
    private ProductPictureService productPictureService;
    private List<ProductPicture> productPictures;

    class PanierList  extends BaseAdapter {
        List<Product> dataSource;

        public PanierList(List<Product> data) {
            //super( activity_client_search.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Product> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateTableList(List<Product> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Product c = dataSource.get(position);


            convertView = LayoutInflater.from(activity_panier.this).inflate(R.layout.row_article_panier, null);
            ImageView picture = convertView.findViewById(R.id.imageView23);
            TextView Name = (TextView) convertView.findViewById(R.id.textView28);
            TextView Price = (TextView) convertView.findViewById(R.id.textView30);

            for (ProductPicture p : productPictures
                 ) {
                if (p.getProductId() == c.getId()){
                    Picasso.get().load(p.getName()).into(picture);
                }
            }

            Name.setText(c.getName());
            for (CommandList a: commandLists
                 ) {
                if (a.getProductId()==c.getId())
                    Price.setText(c.getPrice()+"$ x"+(a.getQuantity()+a.getQuantityOffer()));
            }


            Button Delete = (Button) convertView.findViewById(R.id.button122);

            Delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteProduct = c;
                    commandListService.getCommandList(-1, callBackCommandListget);
                }
            });

            return convertView;
        }
    }
    PanierList PL=null;

    private void Alert(){
        productService.getProduct(-1, callBackProductNotif);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_panier", notificationName, importance);
            channel.setDescription(notificationDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panier);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        idClient = (int)bundle.get(EXTRA_INDEX_CLIENT);
        url = bundle.get(EXTRA_URL).toString();

        commandService = new CommandService(this, url);
        commandListService = new CommandListService(this, url);
        productService = new ProductService(this, url);
        offerService = new OfferService(this, url);
        productPictureService = new ProductPictureService(this, url);
        Alert();

        ImageView chariot = (ImageView) findViewById(R.id.imageView21);
        ImageView personne = (ImageView) findViewById(R.id.imageView22);
        Button button = findViewById(R.id.button66);
        ImageView retour = findViewById(R.id.imageView777);
        list = (ListView) findViewById(R.id.LISTVIEW2);
        prixtotal = (TextView) findViewById(R.id.textView29);

        commandService.getCommand(-1,callBackCommand );
        Button Pay = (Button) findViewById(R.id.button144);

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_client_search.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_command_list.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        Pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentCommand != null) {
                    final Intent intent = new Intent(getApplicationContext(), activity_payment.class);
                    intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                    intent.putExtra(EXTRA_URL, url);
                    startActivityForResult(intent, 1);
                }
            }
        });
        chariot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_panier.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });
        personne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_account_client.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });
    }

    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t:table
                 ) {
                Product p = (Product) t;
                for (CommandList c: commandLists
                     ) {
                    if(c.getProductId() == p.getId()){
                        products.add(p);
                    }
                }
            }

            PL=new PanierList(products);
            list.setAdapter(PL);
            setTotalPrice();
        }


        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandListget = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            delete = null;
            childs = false;
            for (Table t : table
            ) {
                CommandList c = (CommandList)t;
                if(c.getProductId() == deleteProduct.getId()){
                    delete = c;
                    break;
                }
            }

            for (Table t : table
            ) {
                CommandList c = (CommandList)t;
                if(delete.getCommandId() == c.getCommandId() && c.getId() != delete.getId()){
                    childs = true;
                    break;
                }
            }
            commandListService.deleteCommandList(delete.getId(), callBackCommandListdelete);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandListdeleteUpdate = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Toast.makeText(activity_panier.this, "Product deleted", Toast.LENGTH_SHORT).show();
            commandService.getCommand(-1, callBackCommand);

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("command delete ERROR", exception.toString());
        }
    };
    private final CallBack callBackOffer = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getId() == deleteProduct.getOfferId())
                    deleteOffer = o;
            }
            commandService.getCommand(delete.getCommandId(), callBackCommandGet);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("command delete ERROR", exception.toString());
        }
    };

    public int getPrice(Offer offer, Product p, CommandList c){
        int price = 0;
        if (offer.getOfferType() ==0)
            price =p.getPrice()* c.getQuantity();
        if (offer.getOfferType() ==2)
            price =(p.getPrice()* c.getQuantity())- (p.getPrice()* c.getQuantity())*offer.getValue1()/100;
        if (offer.getOfferType() ==1) {
            price = p.getPrice() * c.getQuantity();
        }
        return price;
    }

    private final CallBack callBackCommandGet = new CallBack() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Command c = (Command)table.get(0);
            commandService.putCommand(new Command(delete.getCommandId(),java.time.LocalDate.now().toString(), c.getPrice() - getPrice(deleteOffer, deleteProduct, delete), null, 0, 0, 0, 0 ), callBackCommandListdeleteUpdate);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("command delete ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandListdelete = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            if (childs) {
                offerService.getOffer(-1, callBackOffer);
            }else{
                commandService.deleteCommand(delete.getCommandId(), callBackCommanddelete);
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("command delete ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommanddelete = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Toast.makeText(activity_panier.this, "Product deleted", Toast.LENGTH_SHORT).show();
            commandService.getCommand(-1,callBackCommand );
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductPicture = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productPictures = new ArrayList<>();
            for (Table t : table
            ) {
                ProductPicture c = (ProductPicture) t;
                productPictures.add(c);

            }
            productService.getProduct(-1, callBackProduct);
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("callBackCommandList ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandList = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t : table
                 ) {
                CommandList c = (CommandList)t;
                for (Command cu: currentCommand
                     ) {
                    if(c.getCommandId() == cu.getId()){
                        commandLists.add(c);
                    }
                }

            }
            productPictureService.getProductPicture(-1,callBackProductPicture );
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("callBackCommandList ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommand = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            products = new ArrayList<>();
            currentCommand = new ArrayList<>();
            commandLists = new ArrayList<>();
            for (Table t:table
                 ) {
                Command c = (Command)t;
                if(c.getIdClient() == idClient && c.getStatus().equals("disponible")){
                    currentCommand.add(c);
                }
            }

            commandListService.getCommandList(-1, callBackCommandList);
        }


        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND ERROR", exception.toString());
        }
    };

    private void setTotalPrice(){
        prix=0;
        for (Command p:currentCommand
        ) {
            if (idClient == p.getIdClient()){
                prix+=p.getPrice();
            }
        }
        prixtotal.setText("Total : "+prix);
    }

    /////////////////////////////Notification//////////////////////////////////////
    private final CallBack callBackProductNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productsNotif = new ArrayList<>();
            for (Table t: table
            ) {
                Product o = (Product) t;
                productsNotif.add(o);

            }
            offerService.getOffer(-1,callBackOfferNotif );

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductOfferPut= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOfferNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getNotif() == 1){
                    for (Product p : productsNotif
                    ) {
                        if (o.getId() == p.getOfferId()){
                            notificationDescription += p.getName();
                            createNotificationChannel();//create notification offer for client
                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_panier" )
                                    .setSmallIcon(R.drawable.ic_launcher_background)
                                    .setContentTitle(notificationName)
                                    .setContentText(notificationDescription)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                            // notificationId is a unique int for each notification that you must define
                            notificationManager.notify(5, builder.build());
                            offerService.putOffer(new Offer(o.getId(), null, -1, -1,-1,-1,0),callBackProductOfferPut );
                        }
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}