package com.example.commande;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandListService;
import com.example.commande.service.CommandService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductPictureService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Category;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.CommandList;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.ProductPicture;
import com.example.commande.tableClass.Table;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_client_search.EXTRA_ID_PRODUCT2;

public class activity_search_product extends AppCompatActivity {

    private TextView ArticleName;
    private TextView ArticleStatus;
    private TextView ArticlePrice;
    private ImageView ImageProduct;
    private Button Add;
    private Button Pay;
    private String url;
    private int idProduct;
    private int idClient;
    private ProductService productService;
    private CommandService commandService;
    private CommandListService commandListService;
    private Product currentProduct;
    private EditText editText;
    private boolean add;
    private Offer offer;
    private OfferService offerService;
    private int quantity;
    private Command command;
    private ImageView imageView;
    private ImageView picture;
    private ProductPictureService productPictureService;
    private List<Offer> offers;
    private List<ProductPicture> productPictures;
    private RecyclerView recyclerView;
    private ArrayList<Product>products;
    private activity_search_product.CustomAdapter adapter;
    private List<Product> productsNotif;
    private String notificationName = "New Offer !";
    private String notificationDescription = "There is a new offer for the shop : ";

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);

        public void onLongItemClick(View view, int position);
    }

    public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        private OnItemClickListener mListener;
        GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && mListener != null) {
                        mListener.onLongItemClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
                return true;
            }
            return false;
        }

        @Override public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) { }

        @Override
        public void onRequestDisallowInterceptTouchEvent (boolean disallowIntercept){}
    }

    class CustomAdapter extends RecyclerView.Adapter<activity_search_product.CustomAdapter.CustomViewHolder> {

        private Context context;
        private ArrayList<Product> items;

        public CustomAdapter(Context context, ArrayList<Product> items) {
            this.context = context;
            this.items = items;
        }

        @NonNull
        @Override
        public activity_search_product.CustomAdapter.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new activity_search_product.CustomAdapter.CustomViewHolder(LayoutInflater.from(context).inflate(R.layout.search_row_listview_table_product, parent, false));
        }


        @Override
        public void onBindViewHolder(@NonNull activity_search_product.CustomAdapter.CustomViewHolder holder, int position) {
            Product p = items.get(position);
            holder.nom.setText(p.getName());
            holder.price.setText(p.getPrice()+"$");
            holder.status.setText(p.getStatus());
            for (Offer o:offers
                 ) {
                if (p.getOfferId()==o.getId()){
                    if(o.getOfferType()==2)
                    {
                        holder.offre.setText("-"+o.getValue1()+"%");
                    }
                    else if(o.getOfferType()==1)
                    {
                        holder.offre.setText("Buy "+o.getValue1()+" Get "+o.getValue2());
                    }
                }
            }

            for (ProductPicture pr:productPictures
                 ) {
                if(pr.getProductId() == p.getId()){
                    Picasso.get().load(pr.getName()).into(holder.picture);
                }
            }
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class CustomViewHolder extends RecyclerView.ViewHolder {

            private TextView nom;
            private TextView price;
            private TextView status;
            private TextView offre;
            private ImageView picture;

            public CustomViewHolder(View view) {
                super(view);
                nom = view.findViewById(R.id.Nom);
                price = view.findViewById(R.id.Price);
                status = view.findViewById(R.id.Statut);
                offre = view.findViewById(R.id.Offre);
                picture = view.findViewById(R.id.imageView18);
            }
        }
    }

    private void Alert(){
        productService.getProduct(-1, callBackProductNotif);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_search_product", notificationName, importance);
            channel.setDescription(notificationDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        idClient = (int) bundle.get(EXTRA_INDEX_CLIENT);
        idProduct = (int) bundle.get(EXTRA_ID_PRODUCT2);
        url = bundle.get(EXTRA_URL).toString();

        ArticleName=(TextView) findViewById(R.id.nameArticle);
        ArticleStatus=(TextView)findViewById(R.id.statusArticle);
        ArticlePrice=(TextView) findViewById(R.id.priceArticle);
        ImageProduct=(ImageView) findViewById(R.id.imageView18);
        picture = findViewById(R.id.imageView18);
        Add=(Button) findViewById(R.id.buttonAddArticle);
        Pay=(Button)findViewById(R.id.buttonPayArticle);
        editText = findViewById(R.id.quantity);
        imageView = findViewById(R.id.imageView555);
        recyclerView = findViewById(R.id.recycler_view2);

        commandListService = new CommandListService(this, url);
        commandService = new CommandService(this, url);
        offerService = new OfferService(this, url);
        productService = new ProductService(this, url);
        productPictureService = new ProductPictureService(this, url);
        productService.getProduct(idProduct, callBackProduct);
        Alert();


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), activity_client_search.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });
        Add.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                if(editText.getText().toString() != null) {
                    add = true;
                    if(currentProduct.getStatus().equals("disponible"))
                        offerService.getOffer(-1, callBackOffer);
                }
            }
        });

        Pay.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                add = false;
                if(currentProduct.getStatus().equals("disponible"))
                    offerService.getOffer(-1, callBackOffer);
            }
        });
    }

    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            currentProduct = (Product) table.get(0);
            ArticleName.setText(currentProduct.getName());
            ArticleStatus.setText(currentProduct.getStatus());
            ArticlePrice.setText("Price : "+currentProduct.getPrice());
            productPictureService.getProductPicture(-1, callBackProductPocture);
            offerService.getOffer(-1, callBackOfferrrr);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackOfferrrr = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            offers = new ArrayList<>();
            for (Table t:table
                 ) {
                Offer o = (Offer)t;
                offers.add(o);
            }
            productPictureService.getProductPicture(-1, callBackProductPicture);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductPicture = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productPictures = new ArrayList<>();
            for (Table t:table
            ) {
                ProductPicture p = (ProductPicture) t;
                productPictures.add(p);
            }
            productService.getProduct(-1,callBackProduct777 );

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackProduct777 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            products = new ArrayList<>();
            adapter = new CustomAdapter(getApplicationContext(), products);
            for (Table t:table
            ) {
                Product p = (Product) t;
                if(p.getCategoryId() == currentProduct.getCategoryId() && p.getId() != currentProduct.getId() && p.getIsDeleted() ==0){
                    products.add(p);
                }
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(adapter);

            recyclerView.addOnItemTouchListener(
                    new RecyclerItemClickListener(getApplicationContext(), recyclerView ,new OnItemClickListener() {
                        @Override public void onItemClick(View view, int position) {
                            Intent intent = new Intent(getApplicationContext(), activity_search_product.class);
                            intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                            intent.putExtra(EXTRA_ID_PRODUCT2, products.get(position).getId());
                            intent.putExtra(EXTRA_URL, url);
                            startActivityForResult(intent,1);
                        }

                        @Override public void onLongItemClick(View view, int position) {
                            // do whatever
                        }
                    })
            );
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductPocture = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t :table
                 ) {
                ProductPicture p = (ProductPicture) t;
                if(p.getProductId() == currentProduct.getId()){
                    Picasso.get().load(p.getName()).into(picture);
                }
            }

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackOffer = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t:table
                 ) {
                Offer o = (Offer) t;
                if (currentProduct.getOfferId() == o.getId())
                    offer = o;
            }
            commandService.getCommand(-1, callBackCommandGet);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND get ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandGet = new CallBack() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Command result = null;
            for (Table t:table
            ) {
                Command c = (Command)t;
                if(c.getIdClient() == idClient && c.getIdShop() == currentProduct.getShopId() && c.getStatus().equals("disponible")){
                    result = c;
                }
            }

            if (result != null){
                commandService.putCommand(new Command(result.getId(),java.time.LocalDate.now().toString(), getPrice(0, offer) + result.getPrice(), null, 0, 0, 0, 0 ), callBackCommandPut);
            }else{

                commandService.postCommand(new Command(-1,java.time.LocalDate.now().toString(),getPrice(0, offer), "disponible", idClient, currentProduct.getShopId(), 0, 0), callBackCommandPost);
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND get ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandListGet = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            CommandList result = null;
            for (Table t : table
                 ) {
                CommandList c = (CommandList ) t;
                if (c.getProductId() == idProduct && c.getCommandId() == command.getId())
                    result = c;

            }
            if(result != null)
                commandListService.putCommandList(new CommandList(result.getId(), Integer.valueOf(editText.getText().toString())+ result.getQuantity() , 0, 0,getQuantity(Integer.valueOf(editText.getText().toString()), offer)), callBackCommandListPost);
            else
                commandListService.postCommandList(new CommandList(-1, Integer.valueOf(editText.getText().toString()), command.getId(), currentProduct.getId(), getQuantity(Integer.valueOf(editText.getText().toString()), offer)), callBackCommandListPost);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND get ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            command = (Command) table.get(0);
            commandListService.getCommandList(-1,callBackCommandListGet);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND get ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandPost = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Command result = (Command) table.get(0);
            commandListService.postCommandList(new CommandList(-1, Integer.valueOf(editText.getText().toString()), result.getId(), currentProduct.getId(), getQuantity(Integer.valueOf(editText.getText().toString()), offer)), callBackCommandListPost);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND get ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandListPost = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            if(add)
                Toast.makeText(activity_search_product.this, "Adding product", Toast.LENGTH_SHORT).show();
            else{
                final Intent intent = new Intent(getApplicationContext(), activity_panier.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND get ERROR", exception.toString());
        }
    };

    public int getPrice(int price, Offer offer){
        if (offer.getOfferType() ==0)
            price =currentProduct.getPrice()* Integer.valueOf(editText.getText().toString());
        if (offer.getOfferType() ==2)
            price =(currentProduct.getPrice()* Integer.valueOf(editText.getText().toString()))- (currentProduct.getPrice()* Integer.valueOf(editText.getText().toString()))*offer.getValue1()/100;
        if (offer.getOfferType() ==1) {
            price = currentProduct.getPrice() * Integer.valueOf(editText.getText().toString());
        }
        return price;
    }

    public int getQuantity(int quantity, Offer offer){
        int newQuantity = 0;
        if (offer.getOfferType() ==1){
            if (quantity >= offer.getValue1()){

                if(offer.getValue1()%2 == 0){
                    for(int i =offer.getValue1(); i <= quantity; i =i + offer.getValue1()){
                        if (i %offer.getValue1() != 0)
                            break;
                        newQuantity += offer.getValue2();
                    }
                }else {
                    for (int i = 0; i < quantity; i = i + offer.getValue1()) {
                        newQuantity += offer.getValue2();
                    }
                }
            }
        }
        return newQuantity;
    }

    /////////////////////////////Notification//////////////////////////////////////
    private final CallBack callBackProductNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productsNotif = new ArrayList<>();
            for (Table t: table
            ) {
                Product o = (Product) t;
                productsNotif.add(o);

            }
            offerService.getOffer(-1,callBackOfferNotif );

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductOfferPut= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOfferNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getNotif() == 1){
                    for (Product p : productsNotif
                    ) {
                        if (o.getId() == p.getOfferId()){
                            notificationDescription += p.getName();
                            createNotificationChannel();//create notification offer for client
                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_search_product" )
                                    .setSmallIcon(R.drawable.ic_launcher_background)
                                    .setContentTitle(notificationName)
                                    .setContentText(notificationDescription)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                            // notificationId is a unique int for each notification that you must define
                            notificationManager.notify(7, builder.build());
                            offerService.putOffer(new Offer(o.getId(), null, -1, -1,-1,-1,0),callBackProductOfferPut );
                        }
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };


}
