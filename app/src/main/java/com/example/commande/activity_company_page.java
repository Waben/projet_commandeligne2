package com.example.commande;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductService;
import com.example.commande.service.ShopService;
import com.example.commande.service.TemplateService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.Shop;
import com.example.commande.tableClass.Table;
import com.example.commande.tableClass.Template;

import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_ISCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_configure_template.EXTRA_NAME;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.activity_configure_template.EXTRA_1;
import static com.example.commande.activity_configure_template.EXTRA_2;
import static com.example.commande.activity_configure_template.EXTRA_3;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;

public class activity_company_page extends AppCompatActivity {

    private TextView textView;
    private TextView textView2;
    private TextView textView3;
    private TextView compagnyname;
    private ImageView imageView;
    String name;
    String url;
    String msg1;
    String msg2;
    String msg3;
    int idTemplate;
    int idcommercant;
    private TemplateService templateService;
    private ShopService shopService;
    private int id = -1;
    private Template template;
    private Shop shop;
    private String index1;
    private String index2;
    private boolean isConnexion;
    private int isCommercant;
    private int idClient;
    private List<Product> productsNotif;
    private String notificationName = "New Offer !";
    private String notificationDescription = "There is a new offer for the shop : ";
    private ProductService productService;
    private OfferService offerService;
    private CommandService commandService;
    private String notificationNameCommand = "New Command !";
    private String notificationDescriptionCommand = "There is a new command for your shop !";

    private void Alert(){
        productService.getProduct(-1, callBackProductNotif);
    }

    private void createNotificationChannel2() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_company_page2", notificationNameCommand, importance);
            channel.setDescription(notificationDescriptionCommand);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void AlertCommand(){
        commandService.getCommand(-1, callBackCommandNotif);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_company_page1", notificationName, importance);
            channel.setDescription(notificationDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_page);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        isCommercant = (int) bundle.get(EXTRA_ISCOMMERCANT);
        url = bundle.get(EXTRA_URL).toString();
        templateService= new TemplateService(this, url);
        shopService = new ShopService(this, url);
        offerService = new OfferService(this, url);
        productService = new ProductService(this, url);
        commandService = new CommandService(this, url);
        index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
        index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();

        if(index1 != null){
            idcommercant = Integer.valueOf(index1);
        }else{
            idcommercant = Integer.valueOf(index2);
        }

        if(isCommercant == 1) {

            idTemplate = (int) bundle.get(EXTRA_ID_TEMPLATE);

            if (idTemplate == -1) {
                isConnexion = true;
            } else {

                isConnexion = false;
                msg1 = bundle.get(EXTRA_1).toString();
                msg2 = bundle.get(EXTRA_2).toString();
                msg3 = bundle.get(EXTRA_3).toString();
                name = bundle.get(EXTRA_NAME).toString();
            }
            AlertCommand();


        }else if(isCommercant == 0){
            idClient = (int) bundle.get(EXTRA_INDEX_CLIENT);
            isConnexion = true;
            Alert();

        }
        templateService.getTemplate(-1, callBackTemplateGet);
    }

    private void printTemplate(Template t, Shop s, int templateId, int isCommercant, boolean isConnexion ){
        compagnyname = findViewById(R.id.textView);
        textView = findViewById(R.id.textView6);
        imageView = findViewById(R.id.imageView111);


        if(isConnexion){
            name = s.getShopName();
        }

        compagnyname.setText(name);

        Bundle bundle2 = new Bundle();
        bundle2.putString(EXTRA_INDEX_CCOMMERCANT, index2);
        bundle2.putString(EXTRA_INDEX_COMMERCANT, index1);
        bundle2.putString(EXTRA_URL, url);
        bundle2.putInt(EXTRA_ID_TEMPLATE, idTemplate);
        bundle2.putString(EXTRA_NAME, name);
        bundle2.putInt(EXTRA_ISCOMMERCANT, isCommercant);


        if (isCommercant ==0)
            bundle2.putInt(EXTRA_INDEX_CLIENT, idClient);
        if (t != null) {
            if (t.getInfo1() == null || t.getInfo1().equals("null")) {
                bundle2.putString(EXTRA_1, "No description");
            }else {
                bundle2.putString(EXTRA_1, t.getInfo1());
            }
            if (t.getInfo2() == null || t.getInfo2().equals("null")) {
                bundle2.putString(EXTRA_2, "No description");
            }else {
                bundle2.putString(EXTRA_2, t.getInfo2());
            }
            if (t.getInfo3() == null || t.getInfo3().equals("null")) {
                bundle2.putString(EXTRA_3, "No description");
            }else {
                bundle2.putString(EXTRA_3, t.getInfo3());
            }

        }else{
            bundle2.putString(EXTRA_1, "No description");
            bundle2.putString(EXTRA_2, "No description");
            bundle2.putString(EXTRA_3, "No description");
        }

        if (templateId ==1) {
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view2, fragment_compagny_page_template_1.class, bundle2)
                    .commit();
        }else if(templateId == 2){
            //TODO modifier le fragment
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view2, fragment_compagny_page_template_2.class, bundle2)
                    .commit();
        }else if (templateId == 3){
            //TODO modifier le fragment
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view2, fragment_compagny_page_template_1.class, bundle2)
                    .commit();
        }else{
            //default template
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view2, fragment_compagny_page_template_1.class, bundle2)
                    .commit();
        }

        if (t != null) {
            if (t.getInfo1() == null || t.getInfo1().equals("null"))
                textView.setText("No description");
            else
                textView.setText(t.getInfo1());
        }else
            textView.setText("No description");


        if(isCommercant == 1){


            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
            getSupportFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_container_view, fragment_compagny_page_button.class, bundle2)
                    .commit();

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivityForResult(intent,1);
                }
            });
        }else if(isCommercant == 0){
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), activity_client_search.class);
                    intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                    intent.putExtra(EXTRA_URL, url);
                    startActivityForResult(intent,1);
                }
            });
        }



    }

    private final CallBack callBackTemplateGet = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
                 ) {
                Template t2 = (Template) t;
                if(t2.getShopId() == idcommercant){
                    id = t2.getId();
                    template = t2;
                    break;
                }
            }
            if(isConnexion) {
                if (template != null) {
                    msg1 = template.getInfo1();
                    msg2 = template.getInfo2();
                    msg3 = template.getInfo3();
                    idTemplate = template.getIdTemplate();
                }
            }


            if(!isConnexion) {
                if (msg1.equals(""))
                    msg1 = null;
                if (msg2.equals(""))
                    msg2 = null;
                if (msg3.equals(""))
                    msg3 = null;
                if (id == -1)
                    templateService.postTemplate(new Template(id, idcommercant, idTemplate, msg1, msg2, msg3, null, null), callBackTemplatePut);
                else
                    templateService.putTemplate(new Template(id, idcommercant, idTemplate, msg1, msg2, msg3, null, null), callBackTemplatePut);
            }else{
                if(template != null)
                    shopService.getShop(template.getShopId(), callBackShop);
                else
                    shopService.getShop(idcommercant, callBackShop);
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("TEMPLATE GET ERROR", exception.toString());
        }
    };

    private final CallBack callBackShop = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            shop = (Shop) table.get(0);
            printTemplate(template, shop, idTemplate, isCommercant, isConnexion);

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("TEMPLATE GET ERROR", exception.toString());
        }
    };

    private final CallBack callBackTemplatePut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            template = (Template) table.get(0);
            idTemplate = template.getIdTemplate();
            shopService.getShop(template.getShopId(), callBackShop);

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("TEMPLATE GET ERROR", exception.toString());
        }
    };

    /////////////////////////////Notification client//////////////////////////////////////
    private final CallBack callBackProductNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productsNotif = new ArrayList<>();
            for (Table t: table
            ) {
                Product o = (Product) t;
                productsNotif.add(o);

            }
            offerService.getOffer(-1,callBackOfferNotif );

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductOfferPut= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOfferNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getNotif() == 1){
                    for (Product p : productsNotif
                    ) {
                        if (o.getId() == p.getOfferId()){
                            notificationDescription += p.getName();
                            createNotificationChannel();//create notification offer for client
                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_company_page1" )
                                    .setSmallIcon(R.drawable.ic_launcher_background)
                                    .setContentTitle(notificationName)
                                    .setContentText(notificationDescription)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                            // notificationId is a unique int for each notification that you must define
                            notificationManager.notify(8, builder.build());
                            offerService.putOffer(new Offer(o.getId(), null, -1, -1,-1,-1,0),callBackProductOfferPut );
                        }
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    ////////////////////////////////Notification Command///////////////////////////////////////

    private final CallBack callBackCommandNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Command c = (Command) t;
                if(c.getNotif() == 1){
                    if (c.getIdShop() == idcommercant){
                        createNotificationChannel2();//create notification offer for client
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_company_page2" )
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle(notificationNameCommand)
                                .setContentText(notificationDescriptionCommand)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                        // notificationId is a unique int for each notification that you must define
                        notificationManager.notify(9, builder.build());
                        commandService.putCommand(new Command(c.getId(), null, -1, null, -1, -1, 0, 0),callBackCommandNotifPut );
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandNotifPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}