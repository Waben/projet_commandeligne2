package com.example.commande;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.commande.service.CallBack;
import com.example.commande.service.CategoryService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductPictureService;
import com.example.commande.service.ProductService;
import com.example.commande.service.VolleyMultypartRequest;
import com.example.commande.tableClass.Category;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.ProductPicture;
import com.example.commande.tableClass.Table;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;

public class fragment_adding_stuff_product extends Fragment implements AdapterView.OnItemSelectedListener{

    private static String ROOT_URL;
    private static final int REQUEST_PERMISSIONS = 100;
    private static final int PICK_IMAGE_REQUEST =1 ;
    private Bitmap bitmap;
    private String filePath;
    private ImageView imageView;
    int indexCommercant;
    private Button submit;
    private EditText name;
    private EditText price;
    private ProductService productService;
    private EditText picture;
    private CategoryService categoryService;
    Spinner spinner2;
    int indexSpinner;
    private OfferService offerService;
    AdapterView.OnItemSelectedListener spinnerItem;
    private String url;
    private ProductPictureService productPictureService;
    public fragment_adding_stuff_product() {
        // Required empty public constructor
    }

    public static fragment_adding_stuff_product newInstance(String param1, String param2) {
        fragment_adding_stuff_product fragment = new fragment_adding_stuff_product();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_adding_stuff_product, container, false);
    }
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        price = getView().findViewById(R.id.price);
        name = getView().findViewById(R.id.editTextTextPersonName17);
        submit = getView().findViewById(R.id.button8);
        spinner2 = (Spinner) getView().findViewById(R.id.spinner3);
        picture = getView().findViewById(R.id.picture);
        imageView = getView().findViewById(R.id.imageView);


        spinnerItem = this;

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
            String index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
            url = bundle.get(EXTRA_URL).toString();
            ROOT_URL = url + "/upload";

            if(index1 != null){
                indexCommercant = Integer.valueOf(index1);
            }else{
                indexCommercant = Integer.valueOf(index2);
            }
        }

        productService = new ProductService(this.getActivity(), url);
        categoryService = new CategoryService(this.getActivity(), url);
        offerService = new OfferService(this.getActivity(), url);
        productPictureService = new ProductPictureService(this.getActivity(), url);
        categoryService.getCategory(-1,callBackCategory);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private final CallBack callBackCategory = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            ArrayList<String> CategoryListString=new ArrayList<>();
            for(int i=1;i<table.size();i++) //on prend pas le 1er car cest un test
            {
                Category c = (Category)  table.get(i);
                CategoryListString.add(c.getName());
            }

            ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_list_item_1,CategoryListString);
            adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(adapter2);
            spinner2.setOnItemSelectedListener(spinnerItem);

            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*
                    if ((ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                        System.out.println("premier if");
                        if ((ActivityCompat.shouldShowRequestPermissionRationale(fragment_adding_stuff_product.this.getActivity(),
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(fragment_adding_stuff_product.this.getActivity(),
                                Manifest.permission.READ_EXTERNAL_STORAGE))) {
                            System.out.println("ca marche");

                        } else {
                            ActivityCompat.requestPermissions(fragment_adding_stuff_product.this.getActivity(),
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                                    REQUEST_PERMISSIONS);
                            System.out.println("premier else");
                        }
                    } else {
                        Log.e("Else", "Else");
                        showFileChooser();
                    }


                }*/

                    if (!(price.getText().toString().equals("") || name.getText().toString().equals("") || picture.getText().toString().equals(""))) {
                        indexSpinner = spinner2.getSelectedItemPosition() + 2;
                        offerService.postOffer(new Offer(-1, "rien", 0, 0, 0, indexCommercant, 0), callBackOffre);
                    } else {
                        Toast.makeText(fragment_adding_stuff_product.this.getActivity(), "Enter all informations", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERREOR ", exception.toString());
        }
    };

    private void showFileChooser() {
        if (Build.VERSION.SDK_INT < 19) {
            System.out.println("bouh1");
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(
                    Intent.createChooser(intent, "Select Picture"),
                    PICK_IMAGE_REQUEST);

        } else {
            System.out.println("bouh2");
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, PICK_IMAGE_REQUEST);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("coucou");
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri picUri = data.getData();
            filePath = getPath(picUri);
            if (filePath != null) {
                try {

                    Log.d("filePath", String.valueOf(filePath));
                    bitmap = MediaStore.Images.Media.getBitmap(this.getActivity().getContentResolver(), picUri);
                    uploadBitmap(bitmap);
                    imageView.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                Toast.makeText(
                        fragment_adding_stuff_product.this.getActivity(),"no image selected",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    public String getPath(Uri uri) {
        Cursor cursor = this.getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = this.getActivity().getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void uploadBitmap(final Bitmap bitmap) {

        VolleyMultypartRequest volleyMultipartRequest = new VolleyMultypartRequest(Request.Method.POST, ROOT_URL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Toast.makeText(getContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e("GotError",""+error.getMessage());
                    }
                }) {


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        //adding the request to volley
        Volley.newRequestQueue(this.getContext()).add(volleyMultipartRequest);
    }

    private final CallBack callBackProductPicture = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Toast.makeText(fragment_adding_stuff_product.this.getActivity(), "Product added", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
            Toast.makeText(fragment_adding_stuff_product.this.getActivity(), "Adding product error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Product p = (Product)table.get(0);
            productPictureService.postProductPicture(new ProductPicture(-1, picture.getText().toString(), "re", p.getId(), 2), callBackProductPicture);
            Toast.makeText(fragment_adding_stuff_product.this.getActivity(), "Product added", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
            Toast.makeText(fragment_adding_stuff_product.this.getActivity(), "Adding product error", Toast.LENGTH_SHORT).show();
        }
    };

    private final CallBack callBackOffre = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Product p  = new Product(-1, name.getText().toString(), "disponible", Integer.valueOf(price.getText().toString()), table.get(0).getId(), indexCommercant, indexSpinner, 0);
            productService.postProduct(p, callBackProduct);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("OFFER ERROR", exception.toString());
            Toast.makeText(fragment_adding_stuff_product.this.getActivity(), "Adding product error", Toast.LENGTH_SHORT).show();
        }
    };
}