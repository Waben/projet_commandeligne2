package com.example.commande;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.Table;

import java.util.List;

import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;

public class activity_template_choice extends AppCompatActivity {

    public static String EXTRA_ID_TEMPLATE = "com.example.commande.EXTRA_ID_TEMPLATE";
    String url;
    private String index1;
    private String index2;
    private CommandService commandService;
    private String notificationNameCommand = "New Command !";
    private String notificationDescriptionCommand = "There is a new command for your shop !";

    private void createNotificationChannel2() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_template_choice", notificationNameCommand, importance);
            channel.setDescription(notificationDescriptionCommand);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void AlertCommand(){
        commandService.getCommand(-1, callBackCommandNotif);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_choice);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
        index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
        url = bundle.get(EXTRA_URL).toString();
        commandService = new CommandService(this, url);
        AlertCommand();

        final ImageView imageView = findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), activity_configure_template.class);


               /* final City item = (City) parent.getItemAtPosition(position);
                intent.putExtra(City.TAG, item );*/

                Bundle B=new Bundle();
                B.putInt("nb",1);
                intent.putExtra("data",B);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_ID_TEMPLATE, 1);
                startActivityForResult(intent,1);
            }
        });

        final ImageView imageView1 = findViewById(R.id.imageView4);
        imageView1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), activity_configure_template.class);


               /* final City item = (City) parent.getItemAtPosition(position);
                intent.putExtra(City.TAG, item );*/

                Bundle B=new Bundle();
                B.putInt("nb",2);
                intent.putExtra("data",B);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_ID_TEMPLATE, 2);
                startActivityForResult(intent,1);
            }
        });

        final ImageView imageView2 = findViewById(R.id.imageView5);
        imageView2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), activity_configure_template.class);


               /* final City item = (City) parent.getItemAtPosition(position);
                intent.putExtra(City.TAG, item );*/

                Bundle B=new Bundle();
                B.putInt("nb",3);
                intent.putExtra("data",B);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_ID_TEMPLATE, 3);
                startActivityForResult(intent,1);
            }
        });
    }

    ////////////////////////////////Notification Command///////////////////////////////////////

    private final CallBack callBackCommandNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Command c = (Command) t;
                if(c.getNotif() == 1){
                    if (c.getIdShop() == Integer.valueOf(index1)){
                        createNotificationChannel2();//create notification offer for client
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_template_choice" )
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle(notificationNameCommand)
                                .setContentText(notificationDescriptionCommand)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                        // notificationId is a unique int for each notification that you must define
                        notificationManager.notify(16, builder.build());
                        commandService.putCommand(new Command(c.getId(), null, -1, null, -1, -1, 0, 0),callBackCommandNotifPut );
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandNotifPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}
