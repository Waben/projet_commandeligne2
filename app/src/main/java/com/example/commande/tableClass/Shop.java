package com.example.commande.tableClass;

public class Shop extends Table {
    String shopName;
    String ownerName; //nom
    String ownerSurname; //prenom
    String siret;
    String addresse;
    String mail;
    String contact;

    public Shop(int id, String shopName, String ownerName, String ownerSurname, String siret, String addresse, String mail, String contact) {
        super(id);
        this.shopName = shopName;
        this.ownerName = ownerName;
        this.ownerSurname = ownerSurname;
        this.siret = siret;
        this.addresse = addresse;
        this.mail = mail;
        this.contact = contact;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerSurname() {
        return ownerSurname;
    }

    public void setOwnerSurname(String ownerSurname) {
        this.ownerSurname = ownerSurname;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getAddresse() {
        return addresse;
    }

    public void setAddresse(String addresse) {
        this.addresse = addresse;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "Shop{" + "id =" + id +
                ", shopName='" + shopName + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", ownerSurname='" + ownerSurname + '\'' +
                ", siret='" + siret + '\'' +
                ", addresse='" + addresse + '\'' +
                ", mail='" + mail + '\'' +
                ", contact='" + contact + '\'' +
                '}';
    }
}

