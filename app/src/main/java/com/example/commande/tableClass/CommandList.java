package com.example.commande.tableClass;


public class CommandList extends Table {

    int quantity;

    int commandId;

    int productId;

    int quantityOffer;

    public CommandList(int id, int quantity, int commandId, int productId, int quantityOffer) {
        super(id);
        this.quantity = quantity;
        this.commandId = commandId;
        this.productId = productId;
        this.quantityOffer = quantityOffer;
    }

    public int getQuantityOffer() {
        return quantityOffer;
    }

    public void setQuantityOffer(int quantityOffer) {
        this.quantityOffer = quantityOffer;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getCommandId() {
        return commandId;
    }

    public void setCommandId(int commandId) {
        this.commandId = commandId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "CommandList{" +
                "quantity=" + quantity +
                ", commandId=" + commandId +
                ", productId=" + productId +
                ", quantityOffer=" + quantityOffer +
                ", id=" + id +
                '}';
    }
}

