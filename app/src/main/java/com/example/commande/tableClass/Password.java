package com.example.commande.tableClass;

public class Password extends Table{
    int cliend_id;
    String password;
    int isCommercant;

    public Password(int id, int cliend_id, String password, int isCommercant) {
        super(id);
        this.cliend_id = cliend_id;
        this.password = password;
        this.isCommercant = isCommercant;
    }

    public int getCliend_id() {
        return cliend_id;
    }

    public void setCliend_id(int cliend_id) {
        this.cliend_id = cliend_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIsCommercant() {
        return isCommercant;
    }

    public void setIsCommercant(int isCommercant) {
        this.isCommercant = isCommercant;
    }

    @Override
    public String toString() {
        return "Password{" +
                "cliend_id=" + cliend_id +
                ", password='" + password + '\'' +
                ", isCommercant=" + isCommercant +
                ", id=" + id +
                '}';
    }
}
