package com.example.commande.tableClass;

public class HobbyList extends Table{

    int idHobby;
    int HobbyId;

    public HobbyList(int id, int idHobby, int hobbyId) {
        super(id);
        this.idHobby = idHobby;
        HobbyId = hobbyId;
    }

    public int getIdHobby() {
        return idHobby;
    }

    public void setIdHobby(int idHobby) {
        this.idHobby = idHobby;
    }

    public int getHobbyId() {
        return HobbyId;
    }

    public void setHobbyId(int hobbyId) {
        HobbyId = hobbyId;
    }

    @Override
    public String toString() {
        return "HobbyList{" +
                "idHobby=" + idHobby +
                ", HobbyId=" + HobbyId +
                ", id=" + id +
                '}';
    }
}

