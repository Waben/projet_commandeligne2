package com.example.commande.tableClass;

public class ShopPicture extends Table{

    String name;
    String location;
    int shopId;
    int idTemplate;

    public ShopPicture(int id, String name, String location, int shopId, int idTemplate) {
        super(id);
        this.name = name;
        this.location = location;
        this.shopId = shopId;
        this.idTemplate = idTemplate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getIdTemplate() {
        return idTemplate;
    }

    public void setIdTemplate(int idTemplate) {
        this.idTemplate = idTemplate;
    }

    @Override
    public String toString() {
        return "PictureShop{" +
                "name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", shopId=" + shopId +
                ", idTemplate=" + idTemplate +
                ", id=" + id +
                '}';
    }
}

