package com.example.commande.tableClass;

public class Correspondence extends Table {

    int hobbyId;
    int categoryId;

    public Correspondence(int id, int hobbyId, int categoryId) {
        super(id);
        this.hobbyId = hobbyId;
        this.categoryId = categoryId;
    }

    public int getHobbyId() {
        return hobbyId;
    }

    public void setHobbyId(int hobbyId) {
        this.hobbyId = hobbyId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Correspondence{" +
                "hobbyId=" + hobbyId +
                ", categoryId=" + categoryId +
                ", id=" + id +
                '}';
    }
}

