package com.example.commande.tableClass;

import android.os.Parcel;
import android.os.Parcelable;

public class Command extends Table implements Parcelable {

    public static final String TAG = com.example.commande.tableClass.Command.class.getSimpleName();
    String data;
    int price;
    String status;
    int idClient;
    int idShop;
    int notif;
    int livraison;

    public Command(int id, String data, int price, String status, int idClient, int idShop, int notif, int livraison) {
        super(id);
        this.data = data;
        this.price = price;
        this.status = status;
        this.idClient = idClient;
        this.idShop = idShop;
        this.notif = notif;
        this.livraison = livraison;
    }

    public Command(Parcel in) {
        super(in.readInt());
        this.data = in.readString();
        this.price = in.readInt();
        this.status = in.readString();
        this.idClient = in.readInt();
        this.idShop = in.readInt();
        this.notif = in.readInt();
        this.livraison = in.readInt();
    }

    public int getLivraison() {
        return livraison;
    }

    public void setLivraison(int livraison) {
        this.livraison = livraison;
    }

    public int getNotif() {
        return notif;
    }

    public void setNotif(int notif) {
        this.notif = notif;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getIdShop() {
        return idShop;
    }

    public void setIdShop(int idShop) {
        this.idShop = idShop;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    @Override
    public String toString() {
        return "Command{" +
                "data='" + data + '\'' +
                ", price=" + price +
                ", status='" + status + '\'' +
                ", idClient=" + idClient +
                ", idShop=" + idShop +
                ", notif=" + notif +
                ", livraison=" + livraison +
                ", id=" + id +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(livraison);
        dest.writeString(data);
        dest.writeInt(price);
        dest.writeString(status);
        dest.writeInt(idClient);
        dest.writeInt(idShop);
        dest.writeInt(notif);
    }

    public static final Creator<Command> CREATOR = new Creator<Command>()
    {
        @Override
        public Command createFromParcel(Parcel source)
        {
            return new Command(source);
        }

        @Override
        public Command[] newArray(int size)
        {
            return new Command[size];
        }
    };

}

