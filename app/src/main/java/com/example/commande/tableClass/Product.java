package com.example.commande.tableClass;

import android.os.Parcel;
import android.os.Parcelable;

public class Product extends Table implements Parcelable {

    public static final String TAG = com.example.commande.tableClass.Command.class.getSimpleName();
    String name;
    String status;
    int price;
    int offerId;
    int shopId;
    int categoryId;
    int isDeleted;

    public Product(int id, String name, String status, int price, int offerId, int shopId, int categoryId, int isDeleted) {
        super(id);
        this.name = name;
        this.status = status;
        this.price = price;
        this.offerId = offerId;
        this.shopId = shopId;
        this.categoryId = categoryId;
        this.isDeleted = isDeleted;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", price=" + price +
                ", offerId=" + offerId +
                ", shopId=" + shopId +
                ", categoryId=" + categoryId +
                ", isDeleted=" + isDeleted +
                ", id=" + id +
                '}';
    }

    protected Product(Parcel in) {
        super(in.readInt());
        name = in.readString();
        status = in.readString();
        price = in.readInt();
        offerId = in.readInt();
        shopId = in.readInt();
        categoryId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(status);
        dest.writeInt(price);
        dest.writeInt(offerId);
        dest.writeInt(shopId);
        dest.writeInt(categoryId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

}
