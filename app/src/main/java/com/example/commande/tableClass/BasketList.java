package com.example.commande.tableClass;

public class BasketList extends Table {

    int basketId;
    int quantity;
    int productId;

    public BasketList(int id, int basketId, int quantity, int productId) {
        super(id);
        this.basketId = basketId;
        this.quantity = quantity;
        this.productId = productId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "BasketList{" +
                "basketId=" + basketId +
                ", quantity=" + quantity +
                ", productId=" + productId +
                ", id=" + id +
                '}';
    }
}
