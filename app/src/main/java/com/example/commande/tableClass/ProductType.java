package com.example.commande.tableClass;

public class ProductType extends Table{
    String name;
    int productId;

    public ProductType(int id, String name, int productId) {
        super(id);
        this.name = name;
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "ProductType{" +
                "name='" + name + '\'' +
                ", productId=" + productId +
                ", id=" + id +
                '}';
    }
}

