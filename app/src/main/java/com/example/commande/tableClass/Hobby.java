package com.example.commande.tableClass;

public class Hobby extends Table {

    String name;

    public Hobby(int id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}

