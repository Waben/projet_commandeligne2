package com.example.commande.service;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.commande.tableClass.Client;
import com.example.commande.tableClass.Table;
import com.example.commande.tableClass.Template;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemplateService {

    private static final String TAG = TemplateService.class.getSimpleName();

    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
    //private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static final String CURRENT_TAG = "CURRENT_TEMPLATE";
    private static String URL;
    private RequestQueue queue;

    public TemplateService(@NonNull final Activity activity, String url) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
        URL = url;
    }

    private List<Table> JsonToShopList(JSONObject response)throws Exception{
        final JSONArray userJson = response.getJSONArray("templates");
        List<Table> templates = new ArrayList<>();
        for (int i = 0; i < userJson.length() ; i++) {
            final JSONObject premier = userJson.getJSONObject(i);
            final int id = premier.getInt("id");
            final String info1 = premier.getString("info1");
            final String info2 = premier.getString("info2");
            final String info3 = premier.getString("info3");
            final String info4 = premier.getString("info4");
            final String info5 = premier.getString("info5");
            final int shopId = premier.getInt("boutique_id");
            final int idTemplate = premier.getInt("id_template");
            Template template = new Template(id, shopId, idTemplate, info1, info2, info3, info4, info5);
            templates.add(template);
        }
        return templates;
    }

    private List<Table> JsonToShop(JSONObject response) throws Exception{
        List<Table> templates = new ArrayList<>();
        final int id = response.getInt("id");
        final String info1 = response.getString("info1");
        final String info2 = response.getString("info2");
        final String info3 = response.getString("info3");
        final String info4 = response.getString("info4");
        final String info5 = response.getString("info5");
        final int shopId = response.getInt("boutique_id");
        final int idTemplate = response.getInt("id_template");
        Template template = new Template(id, shopId, idTemplate, info1, info2, info3, info4, info5);
        templates.add(template);
        return templates;
    }

    public void getTemplate(@NonNull final int index, @NonNull final CallBack callback) {
        final String url;
        if(index == -1)
            url = String.format("%s/api/templates", URL);
        else
            url = String.format("%s/api/templates/" + index, URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","GET " + response);
                            if(index == -1)
                                callback.onUpdtate(JsonToShopList(response));
                            else
                                callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void postTemplate(@NonNull final Template template, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/templates", URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("boutique_id", template.getShopId());
            jsonParams.put("id_template", template.getIdTemplate());
            jsonParams.put("info1", template.getInfo1());
            jsonParams.put("info2", template.getInfo2());
            jsonParams.put("info3", template.getInfo3());
            jsonParams.put("info4", template.getInfo4());
            jsonParams.put("info5", template.getInfo5());
        }catch (JSONException e){
            e.printStackTrace();
        }
        //////////////////////
        Log.d(TAG,"Json:"+ jsonParams);
        //////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","POST " + response);

                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void putTemplate(@NonNull Template template, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/templates/" + template.getId(), URL);
        JSONObject jsonParams = new JSONObject();
        try{
            if(template.getInfo1() != null)
                jsonParams.put("info1", template.getInfo1());
            if(template.getInfo2() != null)
                jsonParams.put("info2", template.getInfo2());
            if(template.getInfo3() != null)
                jsonParams.put("info3", template.getInfo3());
            if(template.getInfo4() != null)
                jsonParams.put("info4", template.getInfo4());
            if(template.getInfo5() != null)
                jsonParams.put("info5", template.getInfo5());
            if(template.getShopId() > 0)
                jsonParams.put("boutique_id", template.getShopId());
            if(template.getIdTemplate() > 0)
                jsonParams.put("id_template", template.getIdTemplate());
        }catch (JSONException e){
            e.printStackTrace();
        }
//////////////////////////////////////////////////////
        Log.d("BEFORE","Json:"+ jsonParams);
        ////////////////////////////////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","PUT " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void deleteTemplate(@NonNull final int id, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/templates/"+id, URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_TAG);
    }
}

