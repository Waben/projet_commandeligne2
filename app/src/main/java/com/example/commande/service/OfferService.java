package com.example.commande.service;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.commande.tableClass.Client;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OfferService {

    private static final String TAG = OfferService.class.getSimpleName();

    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
    //private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static final String CURRENT_TAG = "CURRENT_OFFER";
    private static String URL;
    private RequestQueue queue;

    public OfferService(@NonNull final Activity activity, String url) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
        URL = url;
    }

    private List<Table> JsonToShopList(JSONObject response)throws Exception{
        final JSONArray userJson = response.getJSONArray("offres");
        List<Table> offers = new ArrayList<>();
        for (int i = 0; i < userJson.length() ; i++) {
            final JSONObject premier = userJson.getJSONObject(i);
            final int id = premier.getInt("id");
            final String name = premier.getString("name");
            final int offerType = premier.getInt("type_offre");
            final int value1 = premier.getInt("valeur_num1");
            final int value2 = premier.getInt("valeur_num2");
            final int shopId = premier.getInt("boutique_id");
            final int notif = premier.getInt("notif");
            Offer offer = new Offer(id,name,offerType,value1,value2,shopId, notif);
            offers.add(offer);
        }
        return offers;
    }

    private List<Table> JsonToShop(JSONObject response) throws Exception{
        List<Table> offers = new ArrayList<>();
        final int id = response.getInt("id");
        final String name = response.getString("name");
        final int offerType = response.getInt("type_offre");
        final int value1 = response.getInt("valeur_num1");
        final int value2 = response.getInt("valeur_num2");
        final int shopId = response.getInt("boutique_id");
        final int notif = response.getInt("notif");
        Offer offer = new Offer(id,name,offerType,value1,value2,shopId, notif);
        offers.add(offer);
        return offers;
    }

    public void getOffer(@NonNull final int index, @NonNull final CallBack callback) {
        final String url;
        if(index == -1)
            url = String.format("%s/api/offres", URL);
        else
            url = String.format("%s/api/offres/" + index, URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","GET " + response);
                            if(index == -1)
                                callback.onUpdtate(JsonToShopList(response));
                            else
                                callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void postOffer(@NonNull final Offer offer, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/offres", URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("name", offer.getName());
            jsonParams.put("type_offre", offer.getOfferType());
            jsonParams.put("valeur_num1", offer.getValue1());
            jsonParams.put("valeur_num2", offer.getValue2());
            jsonParams.put("boutique_id", offer.getShopId());
            jsonParams.put("notif", offer.getNotif());
        }catch (JSONException e){
            e.printStackTrace();
        }
        //////////////////////
        Log.d(TAG,"Json:"+ jsonParams);
        //////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","POST " + response);

                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void putOffer(@NonNull Offer offer, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/offres/" + offer.getId(), URL);
        JSONObject jsonParams = new JSONObject();
        try{
            if(offer.getName() != null)
                jsonParams.put("name", offer.getName());
            if(offer.getOfferType() >= 0)
                jsonParams.put("type_offre", offer.getOfferType());
            if(offer.getValue1() >= 0)
                jsonParams.put("valeur_num1",offer.getValue1());
            if(offer.getValue2() >= 0)
                jsonParams.put("valeur_num2", offer.getValue2());
            if(offer.getShopId() > 0)
                jsonParams.put("boutique_id", offer.getShopId());
            if(offer.getNotif() == 0 || offer.getNotif() ==1)
                jsonParams.put("notif", offer.getNotif());
        }catch (JSONException e){
            e.printStackTrace();
        }
//////////////////////////////////////////////////////
        Log.d("BEFORE","Json:"+ jsonParams);
        ////////////////////////////////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","PUT " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void deleteOffer(@NonNull final int id, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/offres/"+id, URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_TAG);
    }
}

