package com.example.commande.service;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

import com.example.commande.tableClass.Table;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface CallBack {
    @MainThread
    void onUpdtate(@NonNull final List<Table> table);

    @MainThread
    void onError(@Nullable Exception exception);
}
