package com.example.commande.service;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.commande.tableClass.Client;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.Table;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandService {

    private static final String TAG = CommandService.class.getSimpleName();

    //private static final String URL = "http://192.168.0.35:5000/"; // vitrolles
    //private static final String URL = "http://192.168.137.1:5000/"; // cite u
    private static final String CURRENT_TAG = "CURRENT_COMMAND";
    private static String URL;

    private RequestQueue queue;

    public CommandService(@NonNull final Activity activity, String url) {
        queue = Volley.newRequestQueue(activity.getApplicationContext());
        URL = url;
    }

    private List<Table> JsonToShopList(JSONObject response)throws Exception{
        final JSONArray userJson = response.getJSONArray("commands");
        List<Table> commands = new ArrayList<>();
        for (int i = 0; i < userJson.length() ; i++) {
            final JSONObject premier = userJson.getJSONObject(i);
            final int id = premier.getInt("id");
            final String data = premier.getString("date");
            final String status = premier.getString("status");
            final int price = premier.getInt("prix");
            final int idClient = premier.getInt("client_id");
            final int idShop = premier.getInt("boutique_id");
            final int notif = premier.getInt("notif");
            final int livraison = premier.getInt("livraison");
            Command command = new Command(id,data, price, status, idClient, idShop, notif, livraison);
            commands.add(command);
        }
        return commands;
    }

    private List<Table> JsonToShop(JSONObject response) throws Exception{
        List<Table> commands = new ArrayList<>();
        final int id = response.getInt("id");
        final String data = response.getString("date");
        final String status = response.getString("status");
        final int price = response.getInt("prix");
        final int idClient = response.getInt("client_id");
        final int idShop = response.getInt("boutique_id");
        final int notif = response.getInt("notif");
        final int livraison = response.getInt("livraison");
        Command command = new Command(id,data, price, status, idClient, idShop, notif, livraison);
        commands.add(command);
        return commands;
    }

    public void getCommand(@NonNull final int index, @NonNull final CallBack callback) {
        final String url;
        if(index == -1)
            url = String.format("%s/api/command", URL);
        else
            url = String.format("%s/api/command/" + index, URL);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","GET " + response);
                            if(index == -1)
                                callback.onUpdtate(JsonToShopList(response));
                            else
                                callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void postCommand(@NonNull final Command command, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/command", URL);
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("date", command.getData());
            jsonParams.put("prix", command.getPrice());
            jsonParams.put("status", command.getStatus());
            jsonParams.put("client_id", command.getIdClient());
            jsonParams.put("boutique_id", command.getIdShop());
            jsonParams.put("notif", command.getNotif());
            jsonParams.put("livraison", command.getLivraison());
        }catch (JSONException e){
            e.printStackTrace();
        }
        //////////////////////
        Log.d(TAG,"Json:"+ jsonParams);
        //////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","POST " + response);

                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void putCommand(@NonNull Command command, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/command/" + command.getId(), URL);
        JSONObject jsonParams = new JSONObject();
        try{
            if(command.getData() != null)
                jsonParams.put("date", command.getData());
            if(command.getStatus() != null)
                jsonParams.put("status", command.getStatus());
            if(command.getIdShop() > 0)
                jsonParams.put("boutique_id", command.getIdShop());
            if(command.getPrice() > 0)
                jsonParams.put("prix", command.getPrice());
            if(command.getIdClient() > 0)
                jsonParams.put("client_id", command.getIdClient());
            if(command.getLivraison() > 0)
                jsonParams.put("livraison", command.getLivraison());
            if(command.getNotif() == 0 || command.getNotif() ==1)
                jsonParams.put("notif", command.getNotif());
        }catch (JSONException e){
            e.printStackTrace();
        }
//////////////////////////////////////////////////////
        Log.d("BEFORE","Json:"+ jsonParams);
        ////////////////////////////////////////////////////
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.PUT, url, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","PUT " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            System.out.println("error try");
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String,String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void deleteCommand(@NonNull final int id, @NonNull final CallBack callback) {
        final String url = String.format("%s/api/command/"+id, URL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Tag","ERROR " + response);
                            callback.onUpdtate(JsonToShop(response));
                        } catch (Exception e) {
                            callback.onError(e);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);

                    }
                });
        queue.add(jsonObjectRequest);
    }

    public void cancel() {
        queue.cancelAll(CURRENT_TAG);
    }
}
