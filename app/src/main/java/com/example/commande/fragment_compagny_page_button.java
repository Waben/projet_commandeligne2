package com.example.commande;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.fragment.app.Fragment;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_ISCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_configure_template.EXTRA_1;
import static com.example.commande.activity_configure_template.EXTRA_2;
import static com.example.commande.activity_configure_template.EXTRA_3;
import static com.example.commande.activity_configure_template.EXTRA_NAME;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

public class fragment_compagny_page_button extends Fragment{
    private String index1;
    private String index2;
    private String url;
    private String name;
    private String msg1;
    private String msg2;
    private String msg3;
    private int idTemplate;

    public fragment_compagny_page_button() {
        // Required empty public constructor
    }
    public static fragment_compagny_page_button newInstance(String param1, String param2) {
        fragment_compagny_page_button fragment = new fragment_compagny_page_button();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_compagny_page_button, container, false);
    }
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
            index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
            url = bundle.get(EXTRA_URL).toString();
            idTemplate = (int) bundle.get(EXTRA_ID_TEMPLATE);
            name = bundle.get(EXTRA_NAME).toString();
            msg1 = bundle.get(EXTRA_1).toString();
            msg2 = bundle.get(EXTRA_2).toString();
            msg3 = bundle.get(EXTRA_3).toString();
        }
        final Button button = getView().findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final Intent intent = new Intent(getContext(), activity_adding_stuff.class);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_NAME, name);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                startActivityForResult(intent,1);
            }
        });

        final Button button2 = getView().findViewById(R.id.button3);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final Intent intent = new Intent(getContext(), activity_offers_list.class);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_NAME, name);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                startActivityForResult(intent,1);
            }
        });

        final Button button3 = getView().findViewById(R.id.button5);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Intent intent = new Intent(getContext(), activity_orders_list.class);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_NAME, name);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                startActivityForResult(intent,1);
            }
        });

        final Button button4 = getView().findViewById(R.id.button6);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Intent intent = new Intent(getContext(), activity_template_choice.class);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });
    }
}
