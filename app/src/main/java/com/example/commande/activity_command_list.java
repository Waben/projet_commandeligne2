package com.example.commande;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandListService;
import com.example.commande.service.CommandService;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductService;
import com.example.commande.service.ShopService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.CommandList;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.Shop;
import com.example.commande.tableClass.Table;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_panier.EXTRA_IDCOMMAND;

public class activity_command_list extends AppCompatActivity {
    private int idClient;
    private String url;
    private ListView list2;
    private CommandService commandService;
    private ListView list;
    private ArrayList<Command> commands1;
    private ArrayList<Command> commands2;
    private EditText editText;
    Spinner spinner;
    private String search;
    private ProductService productService;
    private CommandListService commandListService;
    private ShopService shopService;
    private List<Product> products;
    private List<CommandList> commandLists;
    private List<Shop> shops;
    private OfferService offerService;
    private List<Product> productsNotif;
    private String notificationName = "New Offer !";
    private String notificationDescription = "There is a new offer for the shop : ";

    class CommandList1  extends BaseAdapter {
        List<Command> dataSource;

        public CommandList1(List<Command> data) {
            //super( activity_client_search.this, R.layout.row, data);
            dataSource=data;
        }
        public void setData(List<Command> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateTableList(List<Command> newlist) {
            dataSource.clear();
            dataSource.addAll(newlist);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Command c = dataSource.get(position);


            convertView = LayoutInflater.from(activity_command_list.this).inflate(R.layout.row_command_list, null);

            TextView Command = (TextView) convertView.findViewById(R.id.textView37);
            TextView Price = (TextView) convertView.findViewById(R.id.textView36);
            TextView Delivred = (TextView) convertView.findViewById(R.id.textView35);
            TextView date = convertView.findViewById(R.id.textView333);
            Command.setText("Command "+position);
            if (c.getLivraison() == 1)
                date.setText(c.getData() + " by Poste");
            if (c.getLivraison() == 2)
                date.setText(c.getData() + " by delivery guy");
            if (c.getLivraison() == 3)
                date.setText(c.getData() + " by relay point");
            if(c.getStatus().equals("en cours de livraison"))
                Delivred.setText("Not Delivered");
            else {
                if (c.getStatus().equals("send"))
                    Delivred.setText("Delivered");
            }
            Price.setText(c.getPrice()+"");


            Button Delete = (Button) convertView.findViewById(R.id.button15);

            Delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent intent = new Intent(getApplicationContext(), activity_command_view.class);
                    intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                    intent.putExtra(EXTRA_URL, url);
                    intent.putExtra(EXTRA_IDCOMMAND, c.getId());
                    startActivityForResult(intent,1);
                }
            });

            return convertView;
        }
    }
    CommandList1 CL=null;
    CommandList1 CL2=null;

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_command_list", notificationName, importance);
            channel.setDescription(notificationDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void Alert(){
        productService.getProduct(-1, callBackProductNotif);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_command_list);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        url = bundle.get(EXTRA_URL).toString();
        idClient = (int) bundle.get(EXTRA_INDEX_CLIENT);
        commandService = new CommandService(this, url);
        productService = new ProductService(this, url);
        commandListService = new CommandListService(this, url);
        shopService = new ShopService(this, url);
        offerService = new OfferService(this, url);
        Alert();
        commandService.getCommand(-1,callBackCommand);

        ImageView personne = (ImageView) findViewById(R.id.imageView22);
        ImageView chariot = findViewById(R.id.imageView21);
        ImageView retour = findViewById(R.id.imageView777);
        list2 = (ListView) findViewById(R.id.list3);
        list = (ListView) findViewById(R.id.list2);
        spinner = findViewById(R.id.spinner5);
        editText = findViewById(R.id.editTextTextPersonName999);
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0) {
                    for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    }
                    search = s.toString();
                    if (spinner.getSelectedItemPosition() == 0) {
                        //Date
                        commandService.getCommand(-1,callBackCommand2);

                    }
                    if (spinner.getSelectedItemPosition() == 1 ){
                        //Product
                        productService.getProduct(-1,callBackProduct);
                    }

                    if (spinner.getSelectedItemPosition() ==2){
                        //shops
                        shopService.getShop(-1, callBackShop);
                    }
                }else{
                    commandService.getCommand(-1,callBackCommand);
                }
            }
        });
        personne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_account_client.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        chariot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_panier.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_panier.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

    }
    //////////////////////general view///////////////////////////////////////////////
    private final CallBack callBackCommand = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            commands1 = new ArrayList<>();
            commands2 = new ArrayList<>();
            for (Table t : table
                 ) {
                Command c = (Command)t;
                if (c.getIdClient() == idClient){
                    if (c.getStatus().equals("en cours de livraison")){
                        commands1.add(c);
                    }else{
                        if (c.getStatus().equals("send"))
                        commands2.add(c);
                    }
                }
            }

            CL= new CommandList1(commands1);
            list.setAdapter(CL);
            CL2= new CommandList1(commands2);
            list2.setAdapter(CL2);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND ERROR", exception.toString());
        }
    };
    ////////////////research data///////////////////////////////////////////////////////

    private final CallBack callBackCommand2 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            commands1 = new ArrayList<>();
            commands2 = new ArrayList<>();
            for (Table t : table
            ) {
                Command c = (Command)t;
                if (c.getIdClient() == idClient){
                    if (c.getData().startsWith(search)) {
                        if (c.getStatus().equals("en cours de livraison")) {
                            commands1.add(c);
                        } else {
                            if (c.getStatus().equals("send"))
                                commands2.add(c);
                        }
                    }
                }
            }

            CL= new CommandList1(commands1);
            list.setAdapter(CL);
            CL2= new CommandList1(commands2);
            list2.setAdapter(CL2);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND ERROR", exception.toString());
        }
    };

    ////////////////////////research product name///////////////////////////////////////////
    private final CallBack callBackProduct = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            commands1 = new ArrayList<>();
            commands2 = new ArrayList<>();
            products = new ArrayList<>();
            commandLists = new ArrayList<>();
            for (Table t : table
            ) {
                Product p = (Product) t;
                if (p.getName().startsWith(search)){
                    products.add(p);
                }
            }
            commandListService.getCommandList(-1,callBackCommandList );
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandList = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t : table
            ) {
                CommandList c = (CommandList) t;
                for (Product p :products
                     ) {
                    if (c.getProductId() == p.getId()){
                        commandLists.add(c);
                    }
                }
            }
            commandService.getCommand(-1, callBackCommand3);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("Command list ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommand3 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t : table
            ) {
                Command c = (Command)t;
                for (CommandList co: commandLists
                     ) {
                    if (c.getId() == co.getCommandId() && c.getIdClient() == idClient){
                        if (c.getStatus().equals("en cours de livraison")) {
                            commands1.add(c);
                        } else {
                            if (c.getStatus().equals("send"))
                                commands2.add(c);
                        }
                    }
                }
            }

            CL= new CommandList1(commands1);
            list.setAdapter(CL);
            CL2= new CommandList1(commands2);
            list2.setAdapter(CL2);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND ERROR", exception.toString());
        }
    };

    ///////////////////research shop name////////////////////////////

    private final CallBack callBackShop = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            commands1 = new ArrayList<>();
            commands2 = new ArrayList<>();
            shops = new ArrayList<>();
            for (Table t : table
            ) {
                Shop s = (Shop)t;
                if (s.getShopName().startsWith(search)) {
                    shops.add(s);
                }
            }
            commandService.getCommand(-1, callBackCommand4);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommand4 = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t : table
            ) {
                Command c = (Command) t;
                for (Shop s: shops
                     ) {
                    if (c.getIdShop() == s.getId() && c.getIdClient() == idClient){
                        if (c.getStatus().equals("en cours de livraison")) {
                            commands1.add(c);
                        } else {
                            if (c.getStatus().equals("send"))
                                commands2.add(c);
                        }
                    }
                }

            }
            CL= new CommandList1(commands1);
            list.setAdapter(CL);
            CL2= new CommandList1(commands2);
            list2.setAdapter(CL2);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("COMMAND ERROR", exception.toString());
        }
    };

    /////////////////////////////Notification//////////////////////////////////////
    private final CallBack callBackProductNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productsNotif = new ArrayList<>();
            for (Table t: table
            ) {
                Product o = (Product) t;
                productsNotif.add(o);

            }
            offerService.getOffer(-1,callBackOfferNotif );

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductOfferPut= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOfferNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getNotif() == 1){
                    for (Product p : productsNotif
                    ) {
                        if (o.getId() == p.getOfferId()){
                            notificationDescription += p.getName();
                            createNotificationChannel();//create notification offer for client
                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_command_list" )
                                    .setSmallIcon(R.drawable.ic_launcher_background)
                                    .setContentTitle(notificationName)
                                    .setContentText(notificationDescription)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                            // notificationId is a unique int for each notification that you must define
                            notificationManager.notify(3, builder.build());
                            offerService.putOffer(new Offer(o.getId(), null, -1, -1,-1,-1,0),callBackProductOfferPut );
                        }
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}
