package com.example.commande;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.commande.service.CallBack;
import com.example.commande.service.CommandService;
import com.example.commande.service.ShopService;
import com.example.commande.tableClass.Command;
import com.example.commande.tableClass.Shop;
import com.example.commande.tableClass.Table;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_ISCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

public class activity_configure_template extends AppCompatActivity {

    public static String EXTRA_1 = "com.example.command.EXTRA_1";
    public static String EXTRA_2 = "com.example.command.EXTRA_2";
    public static String EXTRA_3 = "com.example.command.EXTRA_3";
    public static String EXTRA_NAME = "com.example.command.NAME";
    String url;
    int idCommercant;
    private ShopService shopService;
    private TextView textView;
    private EditText editText;
    private EditText editText2;
    private  EditText editText3;
    private int idTemplate;
    String shopName;
    private CommandService commandService;
    private String notificationNameCommand = "New Command !";
    private String notificationDescriptionCommand = "There is a new command for your shop !";

    private void createNotificationChannel2() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_configure_template", notificationNameCommand, importance);
            channel.setDescription(notificationDescriptionCommand);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void AlertCommand(){
        commandService.getCommand(-1, callBackCommandNotif);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure_template);

        textView = findViewById(R.id.textView);
        editText = findViewById(R.id.editTextTextPersonName6);
        editText2 = findViewById(R.id.editTextTextPersonName20);
        editText3 = findViewById(R.id.editTextTextPersonName21);
        final Intent intent = getIntent();
        Bundle bundle2 = intent.getExtras();

        String index1 = (String) bundle2.get(EXTRA_INDEX_CCOMMERCANT).toString();
        String index2 = (String) bundle2.get(EXTRA_INDEX_COMMERCANT).toString();
        idTemplate =(int) bundle2.get(EXTRA_ID_TEMPLATE);
        if(index1 != null){
            idCommercant = Integer.valueOf(index1);
        }else{
            idCommercant = Integer.valueOf(index2);
        }
        url = bundle2.get(EXTRA_URL).toString();
        shopService = new ShopService(this, url);
        commandService = new CommandService(this, url);
        AlertCommand();
        shopService.getShop(idCommercant, callBackShop);

        /*final EditText ed=(EditText)findViewById(R.id.editTextTextPersonName6);
        ed.setText(String.valueOf(nb));*/

        ImageView exit = findViewById(R.id.imageView888);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivityForResult(intent,1);
            }
        });

        final Button button = findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                String msg1 = editText.getText().toString();
                String msg2 = editText2.getText().toString();
                String msg3 = editText3.getText().toString();
                final Intent intent = new Intent(getApplicationContext(), activity_company_page.class);
                intent.putExtra(EXTRA_URL, url);
                intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index1);
                intent.putExtra(EXTRA_INDEX_COMMERCANT, index2);
                intent.putExtra(EXTRA_1, msg1);
                intent.putExtra(EXTRA_2, msg2);
                intent.putExtra(EXTRA_3, msg3);
                intent.putExtra(EXTRA_NAME, shopName);
                intent.putExtra(EXTRA_ISCOMMERCANT, 1);
                intent.putExtra(EXTRA_ID_TEMPLATE, idTemplate);
                startActivityForResult(intent,1);
            }
        });
    }

    private final CallBack callBackShop = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Shop s = (Shop)table.get(0);
            shopName =s.getShopName();
            textView.setText(shopName);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("Command ERROR", exception.toString());
        }
    };

    ////////////////////////////////Notification Command///////////////////////////////////////

    private final CallBack callBackCommandNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Command c = (Command) t;
                if(c.getNotif() == 1){
                    if (c.getIdShop() == idCommercant){
                        createNotificationChannel2();//create notification offer for client
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_configure_template" )
                                .setSmallIcon(R.drawable.ic_launcher_background)
                                .setContentTitle(notificationNameCommand)
                                .setContentText(notificationDescriptionCommand)
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                        // notificationId is a unique int for each notification that you must define
                        notificationManager.notify(11, builder.build());
                        commandService.putCommand(new Command(c.getId(), null, -1, null, -1, -1, 0, 0),callBackCommandNotifPut );
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackCommandNotifPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {


        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}