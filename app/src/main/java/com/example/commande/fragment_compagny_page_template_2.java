package com.example.commande;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.commande.service.CallBack;
import com.example.commande.service.OfferService;
import com.example.commande.service.ProductPictureService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.ProductPicture;
import com.example.commande.tableClass.Table;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_ISCOMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.activity_client_search.EXTRA_ID_PRODUCT2;
import static com.example.commande.activity_configure_template.EXTRA_1;
import static com.example.commande.activity_configure_template.EXTRA_2;
import static com.example.commande.activity_configure_template.EXTRA_3;
import static com.example.commande.activity_configure_template.EXTRA_NAME;
import static com.example.commande.activity_template_choice.EXTRA_ID_TEMPLATE;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

public class fragment_compagny_page_template_2 extends Fragment {

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);

        public void onLongItemClick(View view, int position);
    }

    public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        private OnItemClickListener mListener;
        GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && mListener != null) {
                        mListener.onLongItemClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
                return true;
            }
            return false;
        }

        @Override public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) { }

        @Override
        public void onRequestDisallowInterceptTouchEvent (boolean disallowIntercept){}
    }
        class CustomAdapter extends RecyclerView.Adapter<com.example.commande.fragment_compagny_page_template_2.CustomAdapter.CustomViewHolder> {

            private Context context;
            private ArrayList<Offer> items;

            public CustomAdapter(Context context, ArrayList<Offer> items) {
                this.context = context;
                this.items = items;
            }

            @NonNull
            @Override
            public com.example.commande.fragment_compagny_page_template_2.CustomAdapter.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new com.example.commande.fragment_compagny_page_template_2.CustomAdapter.CustomViewHolder(LayoutInflater.from(context).inflate(R.layout.column_compagny_page_offer, parent, false));
            }

            @Override
            public void onBindViewHolder(@NonNull com.example.commande.fragment_compagny_page_template_2.CustomAdapter.CustomViewHolder holder, int position) {
                for (Product p: products
                ) {
                    if (p.getOfferId() ==items.get(position).getId() )
                        holder.text1.setText("Product " + p.getName());
                }

                if(items.get(position).getOfferType()==2)
                {
                    holder.text2.setText("-"+items.get(position).getValue1()+"%");
                }
                else if(items.get(position).getOfferType()==1)
                {
                    holder.text2.setText("Buy "+items.get(position).getValue1()+" Get "+items.get(position).getValue2());
                }
                holder.text3.setText(items.get(position).getName());

            }

            @Override
            public int getItemCount() {
                return items.size();
            }

            public class CustomViewHolder extends RecyclerView.ViewHolder {

                private TextView text1;
                private TextView text2;
                private TextView text3;

                public CustomViewHolder(View view) {
                    super(view);
                    text1 = view.findViewById(R.id.textView2);
                    text2 = view.findViewById(R.id.textView5);
                    text3 = view.findViewById(R.id.textView7);
                }
            }
        }

    class CustomAdapter2 extends RecyclerView.Adapter<fragment_compagny_page_template_2.CustomAdapter2.CustomViewHolder2> {

        private Context context;
        private ArrayList<Product> items;

        public CustomAdapter2(Context context, ArrayList<Product> items) {
            this.context = context;
            this.items = items;
        }

        @NonNull
        @Override
        public fragment_compagny_page_template_2.CustomAdapter2.CustomViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new fragment_compagny_page_template_2.CustomAdapter2.CustomViewHolder2(LayoutInflater.from(context).inflate(R.layout.search_row_listview_table_product, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull fragment_compagny_page_template_2.CustomAdapter2.CustomViewHolder2 holder, int position) {
            Product c = items.get(position);


            holder.Name.setText(c.getName());
            holder.Price.setText(c.getPrice()+" €");
            holder.Status.setText(c.getStatus());

            for (ProductPicture p :productPictures
            ) {
                if (p.getProductId() == c.getId()){
                    Picasso.get().load(p.getName()).into(holder.picture);
                }
            }

            for (Offer o: offer
            ) {
                if (o.getId() == c.getOfferId()){
                    if (o.getOfferType() == 0){
                        holder.Offre.setText("No Offer");
                    }
                    if (o.getOfferType() == 1){
                        holder.Offre.setText("Buy " +o.getValue1()+" Get " + o.getValue2());
                    }
                    if (o.getOfferType() == 2){
                        holder.Offre.setText("-"+o.getValue1()+"%");
                    }
                }
            }

        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class CustomViewHolder2 extends RecyclerView.ViewHolder {

            private TextView Name;
            private TextView Price;
            private TextView Status;
            private TextView Offre;
            private ImageView picture;

            public CustomViewHolder2(View view) {
                super(view);
                Name = view.findViewById(R.id.Nom);
                Price = view.findViewById(R.id.Price);
                Status = view.findViewById(R.id.Statut);
                Offre = view.findViewById(R.id.Offre);
                picture = view.findViewById(R.id.imageView18);
            }
        }
    }
        private com.example.commande.fragment_compagny_page_template_2.CustomAdapter adapter;
    private com.example.commande.fragment_compagny_page_template_2.CustomAdapter2 adapter2;
        private ArrayList<Offer> offers;
        private int indexCommercant;
        private String url;
        private int idClient;
        private int isCommercant;
        private OfferService offerService;
        private ProductService productService;
        private ArrayList<Product> products;
        private ListView list;
        private RecyclerView recyclerView;
        private RecyclerView recyclerView2;
        private List<Offer> offer;
        private TextView textView2;
        private TextView textView3;
        private String msg2;
        private String msg3;
        private String index1;
        String index2;
        String msg1;
        String name;
        private ProductPictureService productPictureService;
        private List<ProductPicture> productPictures;

        public fragment_compagny_page_template_2() {
            // Required empty public constructor
        }

        public static com.example.commande.fragment_compagny_page_template_1 newInstance(String param1, String param2) {
            com.example.commande.fragment_compagny_page_template_1 fragment = new com.example.commande.fragment_compagny_page_template_1();
            Bundle args = new Bundle();

            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null) {

            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_compagny_page_template_2, container, false);
        }

        public void onActivityCreated(Bundle savedInstanceState) {

            super.onActivityCreated(savedInstanceState);
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                index1 = (String) bundle.get(EXTRA_INDEX_CCOMMERCANT).toString();
                index2 = (String) bundle.get(EXTRA_INDEX_COMMERCANT).toString();
                url = bundle.get(EXTRA_URL).toString();
                msg2 = bundle.get(EXTRA_2).toString();
                msg3 = bundle.get(EXTRA_3).toString();
                msg1 = bundle.get(EXTRA_1).toString();
                name = bundle.get(EXTRA_NAME).toString();
                isCommercant = (int)bundle.get(EXTRA_ISCOMMERCANT);
                if(isCommercant == 0)
                    idClient = (int)bundle.get(EXTRA_INDEX_CLIENT);

                if(index1 != null){
                    indexCommercant = Integer.valueOf(index1);
                }else{
                    indexCommercant = Integer.valueOf(index2);
                }
            }

            productService = new ProductService(getActivity(), url);
            offerService = new OfferService(getActivity(), url);
            productPictureService = new ProductPictureService(getActivity(), url);
            textView2 = getView().findViewById(R.id.textView3);
            textView3 = getView().findViewById(R.id.textView4);
            list = (ListView) getView().findViewById(R.id.l);
            recyclerView = getView().findViewById(R.id.recycler_view333);
            recyclerView2 = getView().findViewById(R.id.recycler_view444);

            textView2.setText(msg2);
            textView3.setText(msg3);

            productService.getProduct(-1, callBackProduct);
        }

        private final CallBack callBackProduct = new CallBack() {
            @Override
            public void onUpdtate(@NonNull List<Table> table) {
                products = new ArrayList<>();
                for (Table t:table
                ) {
                    Product p = (Product)t;
                    if (p.getShopId() == indexCommercant && p.getIsDeleted() == 0){
                        products.add(p);
                    }

                }
                productPictureService.getProductPicture(-1, callBackProductPicture);

            }

            @Override
            public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
                Log.e("PRODUCT ERROR", exception.toString());
            }
        };

        private final CallBack callBackProductPicture = new CallBack() {
            @Override
            public void onUpdtate(@NonNull List<Table> table) {
                productPictures = new ArrayList<>();
                for (Table t:table
                ) {
                    ProductPicture p = (ProductPicture) t;
                    productPictures.add(p);

                }
                offerService.getOffer(-1, callBackOffre);

            }

            @Override
            public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
                Log.e("PRODUCT ERROR", exception.toString());
            }
        };

        private final CallBack callBackOffre = new CallBack() {
            @Override
            public void onUpdtate(@NonNull List<Table> table) {
                offers = new ArrayList<>();
                offer = new ArrayList<>();
                adapter2 = new CustomAdapter2(getContext(), products);
                adapter = new com.example.commande.fragment_compagny_page_template_2.CustomAdapter(getContext(), offers);
                for (Table t:table
                ) {
                    Offer o = (Offer)t;
                    if (o.getShopId() == indexCommercant){
                        offer.add(o);
                        if (o.getOfferType() != 0) {
                            offers.add(o);
                        }
                    }

                }
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                recyclerView2.setAdapter(adapter2);
                recyclerView2.addOnItemTouchListener(
                        new RecyclerItemClickListener(getContext(), recyclerView ,new OnItemClickListener() {
                            @Override public void onItemClick(View view, int position) {
                                if(isCommercant ==0){
                                    Product pro = (Product)products.get(position);
                                    Intent intent = new Intent(getContext(), activity_search_product.class);
                                    intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                                    intent.putExtra(EXTRA_ID_PRODUCT2, pro.getId());
                                    intent.putExtra(EXTRA_URL, url);
                                    startActivityForResult(intent,1);
                                }else if( isCommercant == 1){
                                    Product pro = (Product)products.get(position);
                                    Intent intent = new Intent(getContext(), activity_search_product_commercant.class);
                                    intent.putExtra(EXTRA_INDEX_COMMERCANT, index1);
                                    intent.putExtra(EXTRA_INDEX_CCOMMERCANT, index2);
                                    intent.putExtra(EXTRA_ID_PRODUCT2, pro.getId());
                                    intent.putExtra(EXTRA_1, msg1);
                                    intent.putExtra(EXTRA_2, msg2);
                                    intent.putExtra(EXTRA_3, msg3);
                                    intent.putExtra(EXTRA_URL, url);
                                    intent.putExtra(EXTRA_ID_TEMPLATE, 1);
                                    intent.putExtra(EXTRA_NAME, name);
                                    startActivityForResult(intent,1);
                                }
                            }

                            @Override public void onLongItemClick(View view, int position) {
                                // do whatever
                            }
                        })
                );
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
                Log.e("OFFER ERROR", exception.toString());
            }
        };
}
