package com.example.commande;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import static com.example.commande.MainActivity.EXTRA_INDEX_COMMERCANT;
import static com.example.commande.MainActivity.EXTRA_URL;
import static com.example.commande.fragment_commercant_connexion.EXTRA_INDEX_CCOMMERCANT;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link fragment_commandlist_item#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_commandlist_item extends Fragment {


    public fragment_commandlist_item() {
        // Required empty public constructor
    }

    public static fragment_commandlist_item newInstance(String param1, String param2) {
        fragment_commandlist_item fragment = new fragment_commandlist_item();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_commandlist_item, container, false);
    }
}
