package com.example.commande;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.commande.service.CallBack;
import com.example.commande.service.ClientService;
import com.example.commande.service.HobbyListService;
import com.example.commande.service.HobbyService;
import com.example.commande.service.OfferService;
import com.example.commande.service.PasswordService;
import com.example.commande.service.ProductService;
import com.example.commande.tableClass.Client;
import com.example.commande.tableClass.Hobby;
import com.example.commande.tableClass.HobbyList;
import com.example.commande.tableClass.Offer;
import com.example.commande.tableClass.Password;
import com.example.commande.tableClass.Product;
import com.example.commande.tableClass.Table;

import java.util.ArrayList;
import java.util.List;

import static com.example.commande.MainActivity.EXTRA_INDEX_CLIENT;
import static com.example.commande.MainActivity.EXTRA_URL;

public class activity_account_client extends AppCompatActivity {
    class HobbyList  extends BaseAdapter {
        List<Hobby> dataSource;
        List<Boolean> ava;

        public HobbyList(List<Hobby> data, List<Boolean> isActivate) {
            //super(MainActivity.this, R.layout.row, data);
            dataSource=data;
            ava = isActivate;
        }
        public void setData(List<Hobby> d)
        {
            dataSource=d;
        }
        @Override
        public int getCount() {
            return dataSource.size();
        }
        @Override
        public Object getItem(int position) {
            return dataSource.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        public void updateCityList(List<Hobby> newlist, List<Boolean> isActivate) {
            dataSource.clear();
            dataSource.addAll(newlist);
            ava.clear();
            ava.addAll(isActivate);
            this.notifyDataSetChanged();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Hobby c = dataSource.get(position);
            convertView = LayoutInflater.from(activity_account_client.this).inflate(R.layout.row_account_client_hobby,null);

            TextView Text1 = (TextView) convertView.findViewById(R.id.textView31);
            TextView Text2 = (TextView) convertView.findViewById(R.id.textView32);
            ImageView plus = convertView.findViewById(R.id.imageView78);
            ImageView delete = convertView.findViewById(R.id.imageView788);


            Text1.setText(c.getName());
            if(ava.get(position))
                Text2.setText("Activate");
            else
                Text2.setText("Not Activate");

            ImageView exit = findViewById(R.id.imageView888);
            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivityForResult(intent,1);
                }
            });

            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!ava.get(position)){
                        hobbyListService.postHobbyList(new com.example.commande.tableClass.HobbyList(-1, p.getIdHobby(), c.getId()), callBackHobbyListPost);
                    }
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ava.get(position)){
                        currentHobby = c;
                        hobbyListService.getHobbyList(-1, callBackHobbyListDeleteGet);
                    }
                }
            });

            return convertView;
        }
    }
    HobbyList HL=null;

    private  String url;
    private int idClient;
    private ClientService clientService;
    private PasswordService passwordService;
    private HobbyService hobbyService;
    private HobbyListService hobbyListService;
    private EditText first;
    private  EditText last;
    private EditText addresse;
    private EditText mail;
    private EditText mdp;
    private Button button;
    private int idMDP;
    private String md;
    private ImageView imageView;
    private ImageView command;
    private ListView list;
    private List<Boolean> acti;
    private List<Hobby> hobbies;
    private Client p;
    private Hobby currentHobby;
    private List<Product> productsNotif;
    private String notificationName = "New Offer !";
    private String notificationDescription = "There is a new offer for the shop : ";
    private ProductService productService;
    private OfferService offerService;


    private void Alert(){
        productService.getProduct(-1, callBackProductNotif);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("activity_account_client", notificationName, importance);
            channel.setDescription(notificationDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_client);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        first = findViewById(R.id.editTextTextPersonName19);
        last = findViewById(R.id.editTextTextPersonName22);
        addresse = findViewById(R.id.editTextTextPersonName23);
        mail = findViewById(R.id.editTextTextPersonName24);
        mdp = findViewById(R.id.editTextTextPersonName25);
        button = findViewById(R.id.button9);
        imageView = findViewById(R.id.imageView555);
        command = findViewById(R.id.imageView20);
        list = (ListView) findViewById(R.id.l23);

        url = bundle.get(EXTRA_URL).toString();
        idClient = (int)bundle.get(EXTRA_INDEX_CLIENT);

        clientService = new ClientService(this, url);
        passwordService = new PasswordService(this, url);
        hobbyListService = new HobbyListService(this, url);
        hobbyService = new HobbyService(this, url);
        offerService = new OfferService(this, url);
        productService = new ProductService(this, url);
        Alert();
        clientService.getClient(idClient, callBackClientGet);
        hobbyService.getHobby(-1, callBackHobby);


        command.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(getApplicationContext(), activity_panier.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), activity_client_search.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String f = first.getText().toString();
                String l = last.getText().toString();
                String a = addresse.getText().toString();
                md = mdp .getText().toString();
                String m = mail.getText().toString();

                if (f.equals("")){
                    f = null;
                }
                if (l.equals("")){
                    l = null;
                }
                if (a.equals("")){
                    a = null;
                }
                if (md.equals("")){
                    md = null;
                }
                if (m.equals("")){
                    m = null;
                }

                clientService.putClient(new Client(idClient, f, l, m, a, 0, 0),callBackClientPut );
            }
        });

    }

    private final CallBack callBackPasswordPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            Toast.makeText(activity_account_client.this, "Profil updated", Toast.LENGTH_SHORT).show();
            final Intent intent = new Intent(getApplicationContext(), activity_account_client.class);
            intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
            intent.putExtra(EXTRA_URL, url);
            startActivityForResult(intent,1);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

        private final CallBack callBackClientPut = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            if(md != null) {
                passwordService.putPassword(new Password(idMDP, idClient, md, 0), callBackPasswordPut);
            }else{
                Toast.makeText(activity_account_client.this, "Profil updated", Toast.LENGTH_SHORT).show();
                final Intent intent = new Intent(getApplicationContext(), activity_account_client.class);
                intent.putExtra(EXTRA_INDEX_CLIENT, idClient);
                intent.putExtra(EXTRA_URL, url);
                startActivityForResult(intent,1);
            }

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
        };

    private final CallBack callBackClientGet = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            p = (Client) table.get(0);
            first.setHint(p.getName());
            last.setHint(p.getSurname());
            addresse.setHint(p.getAddresse());
            mail.setHint(p.getMail());

            passwordService.getPassword(-1, callBackPasswordGet);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackPasswordGet = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            String passw = "";
            for (Table t: table
                 ) {
                Password p = (Password)t;
                if(p.getCliend_id() == idClient && p.getIsCommercant() == 0){
                    idMDP = p.getId();

                    char[] split = p.getPassword().toCharArray();
                    for (char s: split
                         ) {
                        passw += "*";
                    }
                    break;
                }
            }
            mdp.setHint(passw);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack getCallBackHobbyList = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (int i =0; i< hobbies.size(); i++){
                for (Table t: table
                ) {
                    com.example.commande.tableClass.HobbyList ha = (com.example.commande.tableClass.HobbyList) t;
                    if(hobbies.get(i).getId() == ha.getHobbyId()){
                        if( ha.getIdHobby() == p.getIdHobby()){
                            acti.set(i, true);
                        }
                    }
                }
            }
            HL=new HobbyList(hobbies,acti);

            list.setAdapter(HL);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView parent, View v, int position, long id)
                {
                    acti.set(position,!acti.get(position));
                    HL.updateCityList(hobbies,acti);
                }
            });
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("HOBBYList ERROR", exception.toString());
        }
    };

    private final CallBack callBackHobby = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            hobbies = new ArrayList<>();
            acti = new ArrayList<>();
            for (Table t: table
            ) {
                Hobby p = (Hobby) t;
                hobbies.add(p);
                acti.add(false);
            }
            hobbyListService.getHobbyList(-1, getCallBackHobbyList);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackHobbyListPost = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            hobbyService.getHobby(-1, callBackHobby);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackHobbyListDeleteGet = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            com.example.commande.tableClass.HobbyList result= null;
            for (Table t:table
                 ) {
                com.example.commande.tableClass.HobbyList h = (com.example.commande.tableClass.HobbyList) t;
                if (h.getHobbyId() == currentHobby.getId() && h.getIdHobby() == p.getIdHobby()){
                    result = h;
                    break;
                }
            }
            hobbyListService.deleteHobbyList(result.getId(), callBackHobbyListDelete);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    private final CallBack callBackHobbyListDelete = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            hobbyService.getHobby(-1, callBackHobby);
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("PRODUCT ERROR", exception.toString());
        }
    };

    /////////////////////////////Notification//////////////////////////////////////
    private final CallBack callBackProductNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            productsNotif = new ArrayList<>();
            for (Table t: table
            ) {
                Product o = (Product) t;
                productsNotif.add(o);

            }
            offerService.getOffer(-1,callBackOfferNotif );

        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackProductOfferPut= new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };

    private final CallBack callBackOfferNotif = new CallBack() {
        @Override
        public void onUpdtate(@NonNull List<Table> table) {
            for (Table t: table
            ) {
                Offer o = (Offer) t;
                if(o.getNotif() == 1){
                    for (Product p : productsNotif
                    ) {
                        if (o.getId() == p.getOfferId()){
                            notificationDescription += p.getName();
                            createNotificationChannel();//create notification offer for client
                            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),"activity_account_client" )
                                    .setSmallIcon(R.drawable.ic_launcher_background)
                                    .setContentTitle(notificationName)
                                    .setContentText(notificationDescription)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());

                            // notificationId is a unique int for each notification that you must define
                            notificationManager.notify(2, builder.build());
                            offerService.putOffer(new Offer(o.getId(), null, -1, -1,-1,-1,0),callBackProductOfferPut );
                        }
                    }
                }
            }
        }

        @Override
        public void onError(@org.jetbrains.annotations.Nullable Exception exception) {
            Log.e("CATEGORY ERROR", exception.toString());
        }
    };
}